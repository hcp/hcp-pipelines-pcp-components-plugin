package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.structuralpreprocessing.projectspecific;

import java.util.Date;
import java.util.List;

import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class StructuralPreprocessingPrereqChecker_BWH extends AbstractHcpPipelinesPrereqChecker implements PipelinePrereqCheckerI, ConfigurableComponentI {
		
	// Subclasses may need to override the ones in PipelineConstants
	protected String T1_REGEX = "(?i)T1w_.*_unproc";
	protected String T2_REGEX = PipelineConstants.T2_REGEX;

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		
		// Let's work with a clone here in case the prereq checker sets any values.
		final List<String> exclusions = getExcludedEntities(statusEntity, user);
		if (exclusions.contains(statusEntity.getEntityLabel())) {
			final String infoStr = "Entity has been excluded by configuration"; 
			if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoStr.toString())) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo(infoStr.toString());
			} 
			return;
		}
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			if (!scan.getType().matches("(?i)^T[12]w.*$")) {
				continue;
			}
			if (scan.getQuality().equalsIgnoreCase("POOR")) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo("Session has one or more poor-rated scans.");
				return;
			}
		}
		boolean t1match = false;
		boolean t2match = false;
		for (final XnatAbstractresourceI resource : session.getResources_resource()) {
			if (resource.getLabel().matches(T1_REGEX) && resource.getFileCount()>1) {
				t1match = true;
			} else if (resource.getLabel().matches(T2_REGEX) && resource.getFileCount()>1) {
				t2match = true;
			}
			if (t1match && t2match) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo("Session contains structural unproc resources");
				return;
			}
		}
		final StringBuilder infoSB = new StringBuilder();
		if (!t1match) {
			infoSB.append("Structural T1 resource is missing or empty.   ");
		}
		if (!t2match) {
			infoSB.append("Structural T2 resource is missing or empty.   ");
		}
		statusEntity.setPrereqs(false);
		statusEntity.setPrereqsTime(new Date());
		statusEntity.setPrereqsInfo(infoSB.toString());
	}
	
}
