package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.taskanalysis;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;

@PipelineValidator
public class TaskAnalysisValidator extends AbstractHcpPipelinesValidator {
	

	public static final String ANALYSIS_PROC = "_analysis_proc";
	
	public TaskAnalysisValidator() {
		super();
		EXPECTED_FILE_URL = PipelineConstants.EXPECTEDFILES_FILE_URL + 
			"/TaskAnalysisProcessing.txt";
	}

	@Override
	public String getPipelineName() {
		return PipelineConstants.TASK_ANALYSIS_PIPELINE_NAME;
	}

	@Override
	public String getPipelineResourceLabel(PcpStatusEntity entity) {
		return entity.getSubGroup() + ANALYSIS_PROC; 
	}
	
	@Override
	protected boolean isProcessingComplete(final PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource) {
		final XnatResourcecatalog strucPreprocResource = ResourceUtils.getResource(session, PipelineConstants.MULTIRUNICAFIX_RESOURCE_LABEL);
		if (strucPreprocResource == null) {
			return outcome(entity, "Could not open prerequisite (MultiRunIcaFix) resource", false);
		}
		checkCompletionFileModTime(entity, resource, strucPreprocResource);
		return checkExpectedFiles(entity, session, resource);
	}
	
}
