package org.nrg.ccf.hcppipelines.pcpcomponents.abst;

import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.common.utilities.utils.ScriptExecUtils;
import org.nrg.ccf.commons.utilities.exception.ScriptExecException;
import org.nrg.ccf.hcppipelines.pcpcomponents.exception.PipelineClientException;

public class AbstractPipelineClient {
	
	protected static ScriptResult executeCommand(String cmd) throws PipelineClientException, ScriptExecException {
		ScriptResult result = ScriptExecUtils.execRuntimeCommand(cmd);
		if (!result.isSuccess()) {
			throw new PipelineClientException("ERROR:  Status request execution was not successful");
		}
		return result;
	}
	
}
