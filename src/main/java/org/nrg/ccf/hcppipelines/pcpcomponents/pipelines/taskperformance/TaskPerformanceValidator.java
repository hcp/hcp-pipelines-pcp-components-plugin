package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.taskperformance;

import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class TaskPerformanceValidator implements PipelineValidatorI {
	
	final String TFMRI_UNPROC_REGEX = "(?i)tfMRI_.*_unproc";
	final String STATS_CSV_REGEX = "(?i).*_stats.csv";

	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final List<XnatResourcecatalog> resources = ResourceUtils.getResourcesMatchingRegex(session, TFMRI_UNPROC_REGEX);
		boolean sessionOk = true;
		final StringBuilder infoSB = new StringBuilder();
		for (final XnatResourcecatalog resource : resources) {
			boolean hasMatch = false;
			List<String> fileNames = ResourceUtils.getFileNames(resource);
			for (final String fileName : fileNames) {
				if (fileName.matches(STATS_CSV_REGEX)) {
					hasMatch = true;
					break;
				}
			}
			if (!hasMatch) {
				sessionOk = false;
				infoSB.append("<li>Resource " + resource.getLabel() + " does not have a task stats.csv file</li>");
			}
		}
		if (sessionOk) {
			if (!statusEntity.getValidated()) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo("All tfMRI_*_unproc resources have task stats.csv files.");
			}
		} else {
			if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoSB.toString())) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(infoSB.toString());
			}
		}
	}

}
