package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.multirunicafix;

import java.util.List;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@PipelineValidator
public class MultiRunIcaFixValidator extends AbstractHcpPipelinesValidator {
	
	public MultiRunIcaFixValidator() {
		super();
		EXPECTED_FILE_URL = PipelineConstants.EXPECTEDFILES_FILE_URL + 
			"/MultiRunIcaFixProcessing.txt";
	}

	@Override
	public String getPipelineName() {
		return PipelineConstants.MULTIRUNICAFIX_PIPELINE_NAME;
	}

	@Override
	public String getPipelineResourceLabel(PcpStatusEntity entity) {
		return PipelineConstants.MULTIRUNICAFIX_RESOURCE_LABEL;
	}
	
	@Override
	protected boolean isProcessingComplete(final PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource) {
		final List<XnatResourcecatalog> funcresources = ResourceUtils.getResourcesMatchingRegex(session, PipelineConstants.FUNCTIONAL_PREPROC_RESOURCE_REGEX);
		if (funcresources.isEmpty()) {
			return outcome(entity, "Could not open prerequisite (preproc) resources", false);
		}
		checkCompletionFileModTime(entity, resource, funcresources);
		return checkExpectedFiles(entity, session, resource);
	}

}
