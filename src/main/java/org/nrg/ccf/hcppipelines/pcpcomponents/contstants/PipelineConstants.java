package org.nrg.ccf.hcppipelines.pcpcomponents.contstants;

public class PipelineConstants {
	
	public static final String PCASL_REGEX = "(?i).*PCASL.*_unproc";
	public static final String T1_REGEX = "(?i)T1w_.*_e1e2_mean_unproc";
	public static final String T2_REGEX = "(?i)T2w_.*_unproc";
	public static final String FUNCTIONAL_UNPROC_REGEX = "(?i).*fMRI_.*_unproc";
	public static final String TFMRI_UNPROC_REGEX = "(?i).*tfMRI_.*_unproc";
	public static final String STRUC_PREPROC_RESOURCE_LABEL = "Structural_preproc";
	public static final String STRUC_PREPROC_PIPELINE_NAME = "StructuralPreprocessing";
	public static final String STRUC_HANDEDIT_RESOURCE_LABEL = "Structural_preproc_handedit";
	public static final String STRUC_HANDEDIT_PIPELINE_NAME = "StructuralPreprocessingHandEdit";
	public static final String FUNCTIONAL_PREPROC_PIPELINE_NAME = "FunctionalPreprocessing";
	public static final String FUNCTIONAL_PREPROC_RESOURCE_REGEX = "(?i).*fMRI_.*_preproc";
	public static final String BEDPOSTX_RESOURCE_LABEL = "Diffusion_bedpostx";
	public static final String BEDPOSTX_PIPELINE_NAME = "BedpostxPreprocessing";
	public static final String DIFFUSION_PREPROC_RESOURCE_LABEL = "Diffusion_preproc";
	public static final String DIFFUSION_PREPROC_PIPELINE_NAME = "DiffusionPreprocessing";
	public static final String MULTIRUNICAFIX_PIPELINE_NAME = "MultiRunIcaFixProcessing";
	public static final String MULTIRUNICAFIX_RESOURCE_LABEL = "MultiRunIcaFix_proc";
	public static final String ASL_PIPELINE_NAME = "AslProcessing";
	public static final String ASL_RESOURCE_LABEL = "ASL_proc";
	public static final String AUTORECLEAN_RESOURCE_LABEL = "AutoReclean_proc";
	public static final String AUTORECLEAN_PIPELINE_NAME = "AutoRecleanProcessing";
	// NOTE:  I think we're using AutoReclean instead of ReapplyFix now.
	public static final String REAPPLYFIX_RESOURCE_LABEL = "ReapplyFix_proc";
	public static final String REAPPLYFIX_PIPELINE_NAME = "ReapplyFixProcessing";
	public static final String TICA_RESOURCE_LABEL = "tICA_proc";
	public static final String TICA_PIPELINE_NAME = "TicaProcessing";
	public static final String TASK_ANALYSIS_PIPELINE_NAME = "TaskAnalysisProcessing";
	public static final String TASK_ANALYSIS_RESOURCE_REGEX = "(?i).*fMRI_.*_analysis_proc";
	public static final String MSMALL_PIPELINE_NAME = "MsmAllProcessing";
	public static final String MSMALL_RESOURCE_LABEL = "MsmAll_proc";
	public static final String RUNUTILS_FILE_URL = 
			"https://raw.githubusercontent.com/Washington-University/HCPpipelinesRunUtils/master";
	public static final String XNATPBSJOBS_FILE_URL = 
			"https://raw.githubusercontent.com/Washington-University/HCPpipelinesXnatPbsJobs/master";
	public static final String PIPELINERUNNER_FILE_URL = 
			"https://raw.githubusercontent.com/humanconnectome/hcp-pipelines/master";
	public static final String EXPECTEDFILES_FILE_URL = PIPELINERUNNER_FILE_URL + "/assets/expected_files"; 
	public static final String PIPELINE_OUTPUT_RESOURCES = "(?i).*(_preproc|_proc|bedpostx)$";
	public static final String RESOURCE_LOCKER_PIPELINE_NAME = "ResourceLockerPipeline";

}
