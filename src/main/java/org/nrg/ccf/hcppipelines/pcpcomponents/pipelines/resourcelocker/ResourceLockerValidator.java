package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.resourcelocker;

import java.io.File;
import java.util.Date;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

@PipelineValidator
public class ResourceLockerValidator implements PipelineValidatorI {

	@Override
	public void validate(final PcpStatusEntity statusEntity, final UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StringBuilder infoSB = new StringBuilder();
		final XnatResourcecatalog targetResource = ResourceUtils.getResource(session, statusEntity.getSubGroup());
		final Date statusTime = new Date();
		if (targetResource == null) {
			statusEntity.setValidated(false);
			statusEntity.setIssues(true);
			statusEntity.setValidatedTime(statusTime);
			statusEntity.setIssuesTime(statusTime);
			statusEntity.setValidatedInfo(infoSB.toString());
			statusEntity.setIssuesInfo("Subgroup resource does not exist!");
			return;
		}
		final File resourceCatF = targetResource.getCatalogFile(CommonConstants.ROOT_PATH);
		final File resourceDirF = resourceCatF.getParentFile();
		if (resourceCatF.canWrite() || resourceDirF.canWrite()) {
			statusEntity.setValidated(false);
			statusEntity.setIssues(false);
			statusEntity.setValidatedTime(statusTime);
			statusEntity.setIssuesTime(statusTime);
			statusEntity.setValidatedInfo("Resource is writable.");
			statusEntity.setIssuesInfo("");
		} else {	
			statusEntity.setValidated(true);
			statusEntity.setIssues(false);
			statusEntity.setValidatedTime(statusTime);
			statusEntity.setIssuesTime(statusTime);
			statusEntity.setValidatedInfo("Resource is write locked.");
			statusEntity.setIssuesInfo("");
		}
	}
	
}
