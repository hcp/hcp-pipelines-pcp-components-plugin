package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.taskperformance;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.common.utilities.utils.ScriptExecUtils;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnat.services.archive.CatalogService.Operation;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import com.google.common.collect.ImmutableList;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineSubmitter
public class TaskPerformanceSubmitter implements PipelineSubmitterI {
	
	final static String SCRATCH_PATH = "/data/scratch";
	private final CatalogService _catalogService = XDAT.getContextService().getBean(CatalogService.class);

	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		return ImmutableList.of();
	}

	@Override
	public boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator,
			Map<String, String> parameters, UserI user) throws PcpSubmitException {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(entity.getEntityId(), user, false);
		final String archiveLoc = ArcSpecManager.GetInstance().getArchivePathForProject(entity.getProject());
		final Path archivePath = Paths.get(archiveLoc);
		final List<XnatImagescandataI> scanList = session.getScans_scan();
		boolean success = true;
		for (final XnatImagescandataI scan : scanList) {
			final String scanType = scan.getType();
			if (scanType == null || !scanType.equals("tfMRI")) {
				continue;
			}
			XnatResourcecatalog linkedDataResource = ResourceUtils.getResource(scan, CommonConstants.LINKED_DATA_RESOURCE);
			if (linkedDataResource == null) {
				continue;
			}
			final List<File> wideCsvLst = ResourceUtils.getFilesByFileNameRegex(linkedDataResource, "(?i).*_wide.csv$");
			for (final File wideCsv : wideCsvLst) {
				final File wideCsvDir = wideCsv.getParentFile();
				String wideCsvPath;
				String wideCsvDirPath;
				try {
					wideCsvPath = wideCsv.getCanonicalPath();
					wideCsvDirPath = wideCsvDir.getCanonicalPath();
				} catch (IOException e) {
					wideCsvPath = wideCsv.getAbsolutePath();
					wideCsvDirPath = wideCsvDir.getAbsolutePath();
				}
				final String statsCsvPath = wideCsvPath.replaceFirst("(?i)_wide.csv", "_stats.csv");
				final String scratchWideCsvPath = wideCsvPath.replace(wideCsvDirPath, SCRATCH_PATH);
				final String scratchStatsCsvPath = statsCsvPath.replace(wideCsvDirPath, SCRATCH_PATH);
				final StringBuilder cmdSB = new StringBuilder("singularity exec -B /data/intradb/singularity,");
				cmdSB.append(wideCsvDirPath);
				cmdSB.append(":/data/scratch /data/intradb/singularity/container/intradb-centos7-python3-chpc3.sif ");
				cmdSB.append("/data/intradb/singularity/intradb-singularity/intradb/task_performance_scripts/score_performance_");
				cmdSB.append(getProjectPart(entity.getProject()));
				cmdSB.append("_");
				cmdSB.append(getTaskName(scan));
				cmdSB.append(".py --logfile ");
				cmdSB.append(scratchWideCsvPath);
				cmdSB.append(" --outfile ");
				cmdSB.append(scratchStatsCsvPath);
				log.info("Executing command:  " + cmdSB.toString());
				final ScriptResult result = ScriptExecUtils.execRuntimeCommand(cmdSB.toString());
				if (!result.isSuccess()) {
					success = false;
					continue;
				}
				final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(session.getProject())
						.append("/subjects/").append(session.getSubjectId()).append("/experiments/").append(session.getId())
						.append("/scans/").append(scan.getId())
						.append("/resources/").append(CommonConstants.LINKED_DATA_RESOURCE);
				try {
					_catalogService.refreshResourceCatalog(user, resourceSB.toString(), Operation.ALL);
				} catch (ServerException | ClientException e) {
					success = false;
				}
				if (!success) {
					continue;
				}
				// Update session unproc resource (create symlink)
				File statsCsv = new File(statsCsvPath);
				if (!statsCsv.exists()) {
					continue;
				}
				final XnatResourcecatalog sessionResource = ResourceUtils.getResource(session, scan.getSeriesDescription() + "_unproc");
				final List<File> sessionWideCsvLst = ResourceUtils.getFilesByFileNameRegex(sessionResource, "(?i).*_wide.csv$");
				for (final File sessionWideCsv : sessionWideCsvLst) {
					final File sessionWideCsvDir = sessionWideCsv.getParentFile();
					String sessionWideCsvPath = sessionWideCsv.getAbsolutePath();
					final String sessionStatsCsvPath = sessionWideCsvPath.replaceFirst("(?i)_wide.csv", "_stats.csv");
					try {
						createLink(new File(sessionStatsCsvPath).toPath(), statsCsv.toPath(), archivePath);
						final StringBuilder sessionResourceSB = new StringBuilder("/archive/projects/").append(session.getProject())
								.append("/subjects/").append(session.getSubjectId()).append("/experiments/").append(session.getId())
								.append("/resources/").append(scan.getSeriesDescription()).append("_unproc");
						try {
							_catalogService.refreshResourceCatalog(user, sessionResourceSB.toString(), Operation.ALL);
						} catch (ServerException | ClientException e) {
							success = false;
						}
					} catch (IOException e) {
						success = false;
						log.error("ERROR:  Could not create symlink (" + statsCsvPath + " to " + sessionStatsCsvPath + ")");
					}
				}
			}
		}
		if (success) {
			entity.setStatus(PcpConstants.PcpStatus.COMPLETE);
			entity.setStatusInfo("Tast stats generation script reported successuful completion");
		} else {
			entity.setStatus(PcpConstants.PcpStatus.ERROR);
			entity.setStatusInfo("Tast stats generation script execution was not successuful");
		}
		entity.setStatusTime(new Date());
		entityService.update(entity);
		return success;
	}

	private String getProjectPart(String project) {
		return project.replaceFirst("CCF_", "").replaceFirst("_STG$", "");
	}

	private String getTaskName(XnatImagescandataI scan) {
		return scan.getSeriesDescription().replaceFirst("(?i)tfMRI_", "").replaceFirst("_[AP][PA]$", "");
	}
	

	private void createLink(Path linkedFP, Path targetP, Path archivePath) throws IOException {
	    // If we're creating symlinks from the archive to the archive, let's create relative links.
		Files.deleteIfExists(linkedFP);
	    if (!(linkedFP.startsWith(archivePath) && targetP.startsWith(archivePath))) {
	    	Files.createSymbolicLink(linkedFP, targetP);
	    } else {
	    	final Path relTargetP = linkedFP.getParent().relativize(targetP);
	    	Files.createSymbolicLink(linkedFP, relTargetP);
	    }
	}

}
