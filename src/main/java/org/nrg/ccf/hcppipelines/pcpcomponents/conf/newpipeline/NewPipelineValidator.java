package org.nrg.ccf.hcppipelines.pcpcomponents.conf.newpipeline;

import java.util.Date;

import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xft.security.UserI;

//@PipelineValidator
public class NewPipelineValidator implements PipelineValidatorI {
	
	@Override
	public void validate(PcpStatusEntity entity, UserI user) {
		entity.setValidated(false);
		entity.setValidatedInfo("Validation functionality is not yet implemented");
		entity.setValidatedTime(new Date());
	}
	
}
