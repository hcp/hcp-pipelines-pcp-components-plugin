package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.asl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelinePrereqChecker
public class AslPrereqChecker extends AbstractHcpPipelinesPrereqChecker implements ConfigurableComponentI {
	
	protected String PCASL_REGEX;
	protected final String _className = this.getClass().getName().replace(".", "");
	
	public AslPrereqChecker() {
		super();
		PREREQ_PIPELINE_NAME = PipelineConstants.MSMALL_PIPELINE_NAME;
		PREREQ_PIPELINE_PACKAGES = 
				Arrays.asList(new String[] { 
						"org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.msmall" 
				});
	}
	
	public List<String> getConfigurationYaml() {
		List<String> configYaml = new ArrayList<>();
		final StringBuilder sb = new StringBuilder()
				.append("pcaslRegex:\n")
				.append("    id: " + _className + "-pcasl-regex\n")
				.append("    name: " + _className + "-pcasl-regex\n")
				.append("    kind: panel.input.text\n")
				.append("    placeholder: " + PipelineConstants.PCASL_REGEX + "\n")
				.append("    label: Regular expression for matching T1 unproc resources\n")
				;
		configYaml.add(sb.toString());
		configYaml.addAll(super.getConfigurationYaml());
		return configYaml;
	}
	
	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		super.checkPrereqs(statusEntity, user);
		if (!statusEntity.getPrereqs()) {
			return;
		}
		try {
			PCASL_REGEX = PcpComponentUtils.getConfigValue(statusEntity.getProject(), statusEntity.getPipeline(), 
							this.getClass().getName(), "-pcasl-regex", false).toString();
		} catch (PcpAutomationScriptExecutionException e) {
			log.warn("Exception thrown obtaining config values (PCASL_REGEX");
		}
		if (PCASL_REGEX == null || PCASL_REGEX.isEmpty()) {
			PCASL_REGEX = PipelineConstants.PCASL_REGEX;
		}
		
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		boolean pcaslMatch = false;
		for (final XnatAbstractresourceI resource : session.getResources_resource()) {
			// MRH:  Removed filecount requirement because sometimes filecount is off.  
			// Would rather process and have errors than return prereqs=False just because of filecount issues.
			//if (resource.getLabel().matches(PCASL_REGEX) && resource.getFileCount()>1) {
			if (resource.getLabel().matches(PCASL_REGEX)) {
				pcaslMatch = true;
			}
			if (pcaslMatch) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo("Session contains PCASL unproc resources");
				return;
			}
		}
		final StringBuilder infoSB = new StringBuilder();
		if (!pcaslMatch) {
			infoSB.append("PCASL resource is missing or empty.   ");
		}
		statusEntity.setPrereqs(false);
		statusEntity.setPrereqsTime(new Date());
		statusEntity.setPrereqsInfo(infoSB.toString());
		
	}

}
