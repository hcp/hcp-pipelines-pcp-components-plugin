package org.nrg.ccf.hcppipelines.pcpcomponents.client;

import java.util.Iterator;
import java.util.List;

import org.apache.axis2.addressing.AddressingConstants.Final;
import org.nrg.ccf.commons.utilities.exception.ScriptExecException;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractPipelineClient;
import org.nrg.ccf.hcppipelines.pcpcomponents.exception.PipelineClientException;
import org.nrg.ccf.hcppipelines.pcpcomponents.pojo.SubmissionStatus;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;

public class PipelineStatusClient extends AbstractPipelineClient {
	
	final static String PIPELINE_STATUS_COMMAND = System.getProperty("user.home") + "/bin/pipelineStatusChecker";

	public static Integer getQueuedCount() throws PipelineClientException, ScriptExecException {
		return executeCommand(PIPELINE_STATUS_COMMAND + " QUEUED_COUNT").getIntegerResult();
	}

	public static Integer getRunningCount() throws PipelineClientException, ScriptExecException {
		return executeCommand(PIPELINE_STATUS_COMMAND + " RUNNING_COUNT").getIntegerResult();
	}

	public static Integer getQueuedOrRunningCount() throws PipelineClientException, ScriptExecException {
		return executeCommand(PIPELINE_STATUS_COMMAND + " QUEUED_OR_RUNNING_COUNT").getIntegerResult();
	}

	public static Integer getQueuedCount(String pipeline) throws PipelineClientException, ScriptExecException {
		return getQueuedJobs(pipeline).size();
	}

	public static Integer getRunningCount(String pipeline) throws PipelineClientException, ScriptExecException {
		return getRunningJobs(pipeline).size();
	}

	public static Integer getQueuedOrRunningCount(String pipeline) throws PipelineClientException, ScriptExecException {
		return getActiveJobs(pipeline).size();
	}

	public static List<String> getRunningJobs(String pipeline) throws PipelineClientException, ScriptExecException {
		final List<String> runningJobs = getRunningJobs();
		final Iterator<String> i = runningJobs.iterator();
		while (i.hasNext()) {
			if (!i.next().contains("." + pipeline)) {
				i.remove();
			}
		}
		return runningJobs;
	}

	public static List<String> getQueuedJobs(String pipeline) throws PipelineClientException, ScriptExecException {
		final List<String> queuedJobs = getQueuedJobs();
		final Iterator<String> i = queuedJobs.iterator();
		while (i.hasNext()) {
			if (!i.next().contains("." + pipeline)) {
				i.remove();
			}
		}
		return queuedJobs;
	}

	public static List<String> getActiveJobs(String pipeline) throws PipelineClientException, ScriptExecException {
		final List<String> activeJobs = getActiveJobs();
		final Iterator<String> i = activeJobs.iterator();
		while (i.hasNext()) {
			if (!i.next().contains("." + pipeline)) {
				i.remove();
			}
		}
		return activeJobs;
	}

	public static List<String> getRunningJobs() throws PipelineClientException, ScriptExecException {
		return executeCommand(PIPELINE_STATUS_COMMAND + " RUNNING_JOBS").getListResult();
	}

	public static List<String> getQueuedJobs() throws PipelineClientException, ScriptExecException {
		return executeCommand(PIPELINE_STATUS_COMMAND + " QUEUED_JOBS").getListResult();
	}

	public static List<String> getActiveJobs() throws PipelineClientException, ScriptExecException {
		return executeCommand(PIPELINE_STATUS_COMMAND + " ACTIVE_JOBS").getListResult();
	}

	public static SubmissionStatus getJobStatus(final PcpCondensedStatusI entity, final String pipeline) {
		try {
			return SubmissionStatus.valueOf(executeCommand(PIPELINE_STATUS_COMMAND + " JOB_STATUS " + entity.getEntityLabel() +
					" " + entity.getSubGroup() + " " + pipeline).getStringResult());
		} catch (Exception e) {
			return SubmissionStatus.UNKNOWN;
		}
	}

	public static SubmissionStatus getJobStatus(final String entityLabel) {
		try {
			return SubmissionStatus.valueOf(executeCommand(PIPELINE_STATUS_COMMAND + " JOB_STATUS " + entityLabel).getStringResult());
		} catch (Exception e) {
			return SubmissionStatus.UNKNOWN;
		}
	}

	public static String getDetailedJobStatus(final PcpCondensedStatusI entity, final String pipeline) {
		try {
			return "<pre>" + executeCommand(PIPELINE_STATUS_COMMAND + " DETAILED_STATUS " + entity.getEntityLabel() +
					" " + entity.getSubGroup() + " " + pipeline).getStringResult() + "</pre>";
		} catch (Exception e) {
			return "ERROR:  Could not determine submission status of " + entity.getEntityLabel();
		}
	}

	public static String getDetailedJobStatus(final String entityLabel) {
		try {
			return "<pre>" + executeCommand(PIPELINE_STATUS_COMMAND + " DETAILED_STATUS " + entityLabel).getMultilineStringResult() + "</pre>";
		} catch (Exception e) {
			return "ERROR:  Could not determine submission status of " + entityLabel;
		}
	}

}
