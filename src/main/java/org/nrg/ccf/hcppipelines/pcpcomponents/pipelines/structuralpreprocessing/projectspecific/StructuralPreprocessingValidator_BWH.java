package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.structuralpreprocessing.projectspecific;


import org.nrg.ccf.hcppipelines.pcpcomponents.inter.HcpPipelineValidatorI;
import org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.structuralpreprocessing.StructuralPreprocessingValidator;
import org.nrg.ccf.pcp.anno.PipelineValidator;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@PipelineValidator
public class StructuralPreprocessingValidator_BWH extends StructuralPreprocessingValidator implements HcpPipelineValidatorI {
	
	public StructuralPreprocessingValidator_BWH() {
		super();
		T1_REGEX = "(?i)T1w_.*_unproc";
	}
	
}
