package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.tica;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@PipelineValidator
public class TicaProcessingValidator extends AbstractHcpPipelinesValidator {
	
	public TicaProcessingValidator() {
		super();
		EXPECTED_FILE_URL = PipelineConstants.EXPECTEDFILES_FILE_URL + 
			"/TicaProcessing.txt";
	}

	@Override
	public String getPipelineName() {
		return PipelineConstants.TICA_PIPELINE_NAME;
	}

	@Override
	public String getPipelineResourceLabel(PcpStatusEntity entity) {
		return PipelineConstants.TICA_RESOURCE_LABEL;
	}
	
	@Override
	protected boolean isProcessingComplete(final PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource) {
		// Using AutoReclean instead of ReApplyFix now.  Earlier runs were run outside of PCP, so I
		// don't think we need to preservere the ReapplyFix stuff
		//final XnatResourcecatalog procResource = ResourceUtils.getResource(session, PipelineConstants.REAPPLYFIX_RESOURCE_LABEL);
		final XnatResourcecatalog procResource = ResourceUtils.getResource(session, PipelineConstants.AUTORECLEAN_RESOURCE_LABEL);
		if (procResource == null) {
			//return outcome(entity, "Could not open prerequisite (ReapplyFix) resource", false);
			return outcome(entity, "Could not open prerequisite (AutoReclean) resource", false);
		}
		checkCompletionFileModTime(entity, resource, procResource);
		return checkExpectedFiles(entity, session, resource);
	}

}
