package org.nrg.ccf.hcppipelines.pcpcomponents.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.nrg.ccf.common.utilities.components.NodeUtils;
import org.nrg.ccf.commons.utilities.exception.ScriptExecException;
import org.nrg.ccf.hcppipelines.pcpcomponents.client.PipelineStatusClient;
import org.nrg.ccf.hcppipelines.pcpcomponents.client.PipelineSubmissionClient;
import org.nrg.ccf.hcppipelines.pcpcomponents.exception.PipelineClientException;
import org.nrg.ccf.hcppipelines.pcpcomponents.inter.HcpPipelineValidatorI;
import org.nrg.ccf.hcppipelines.pcpcomponents.pojo.SubmissionInfo;
import org.nrg.ccf.hcppipelines.pcpcomponents.pojo.SubmissionStatus;
import org.nrg.ccf.ndatransfer.components.utils.CinabUtils;
import org.nrg.ccf.pcp.abst.PipelineExecManagerA;
import org.nrg.ccf.pcp.anno.PipelineExecManager;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PipelineExecManagerI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.python.google.common.collect.ImmutableList;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineExecManager
public class HcpPipelinesExecManager extends PipelineExecManagerA implements PipelineExecManagerI, ConfigurableComponentI {
	
	final private static long SECONDS = 1000;
	final private static long MINUTES = 60*SECONDS;
	final private static long HOURS = 60*MINUTES;
	final private static long DAYS = 24*HOURS;
	final private Integer MAX_ALLOW_QUEUED = 20;
	private static boolean REQUEST_STOP_MONITORING = false;
	private static boolean REQUEST_STOP_MONITORING_ANY_PIPELINE = false;
	private static Set<String> REQUEST_STOP_PIPELINE_SET = new HashSet<>();
	private int _prevListSize = 0;
	private int _zeroCountdown = -1;
	
	protected final String _className = this.getClass().getName().replace(".", "");
	final PcpStatusEntityService _statusEntityService = XDAT.getContextService().getBean(PcpStatusEntityService.class);
	final NodeUtils _nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
	final CinabUtils _cinabUtils = XDAT.getContextService().getBean(CinabUtils.class);
	final PcpUtils _pcpUtils = XDAT.getContextService().getBean(PcpUtils.class);
	
	public static void issueStopMonitoringRequest() {
		REQUEST_STOP_MONITORING = true;
		REQUEST_STOP_MONITORING_ANY_PIPELINE = true;
		REQUEST_STOP_PIPELINE_SET.clear();
	}
	
	public static void issueStopMonitoringRequest(String pipeline) {
		REQUEST_STOP_MONITORING = true;
		REQUEST_STOP_MONITORING_ANY_PIPELINE = false;
		REQUEST_STOP_PIPELINE_SET.add(pipeline);
	}
	
	@Getter
	public class PipelineInfo {
		private String pipeline;
		private String pipelineArgs;
		private PipelineValidatorI validator;
		private String pipelineName;
		public PipelineInfo(String pipeline, String pipelineArgs, PipelineValidatorI validator) {
			super();
			this.pipeline = pipeline;
			this.pipelineArgs = pipelineArgs;
			this.validator = validator;
			pipelineName = (validator instanceof HcpPipelineValidatorI) ? 
					((HcpPipelineValidatorI)validator).getPipelineName() : pipeline; 
		}
	}

	@Override
	public List<String> getConfigurationYaml() {
		return ImmutableList.of();
	}

	@Override
	public void doSubmit(PipelineSubmitterI submitter, PipelineValidatorI validator, List<PcpCondensedStatusI> statusList,
			Map<String, String> parameters, UserI user) {
		Collections.sort(statusList);
		final String currentPipeline = statusList.get(0).getPipeline();
		String pipelineArgs = parameters.get("pipelineArgs");
		pipelineArgs = (pipelineArgs == null) ? "" : pipelineArgs.trim();
		final PipelineInfo pipelineInfo = new PipelineInfo(currentPipeline, pipelineArgs, validator);
		Integer queuedLimit;
		try {
			queuedLimit = Integer.parseInt(parameters.get("queuedLimit"));
		} catch (Exception e) {
			queuedLimit = MAX_ALLOW_QUEUED;
		}
		log.info("HCPPipelinesExecManager:  Begin Submission (Pipeline=" + currentPipeline + 
				", PipelineArgs='" + pipelineArgs + "'" + 
				", NumberSubmitted=" + statusList.size() + ", QueuedLimit=" + queuedLimit + 
				", OverrideQueuedLimit=" + 
				Boolean.parseBoolean(parameters.get("overrideQueuedLimit")) +
				") - Mark entities SUBMITTED");
		for (final PcpCondensedStatusI entity : statusList) {
			final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(entity);
			if (statusEntity == null) {
				continue;
			}
			statusEntity.setStatus(PcpConstants.PcpStatus.SUBMITTED.toString());
			final Date statusTime = new Date();
			statusEntity.setStatusTime(statusTime);
			statusEntity.setStatusInfo("Submitted for processing at " + statusTime +
			 		".  (NODE=" + _nodeUtils.getXnatNode() + ", PIPELINE=" + pipelineInfo.getPipeline() + ", PIPELINE_ARGS=" + pipelineInfo.getPipelineArgs() + ")");
			_statusEntityService.update(statusEntity);
		}
		final Map<PcpCondensedStatusI, SubmissionInfo> submissionMap = new HashMap<>();
		final Map<PcpCondensedStatusI, String> errorMap = new HashMap<>();
		String failureInfo = "";
		boolean submitsFailed = false;
		log.info("HCPPipelinesExecManager:  Begin Submission (Pipeline=" + currentPipeline + 
				", PipelineArgs='" + pipelineArgs + "'" + 
				", NumberSubmitted=" + statusList.size() + ", QueuedLimit=" + queuedLimit + 
				", OverrideQueuedLimit=" + Boolean.parseBoolean(parameters.get("overrideQueuedLimit")) +
				") - Loop over entities for submission.");
		
		List<String> activeList;
		boolean generateCinabStructure = (Boolean.parseBoolean(parameters.get("generateCinabStructure")));
		try {
			activeList = (Boolean.parseBoolean(parameters.get("resetCurrentActive"))) ? 
					PipelineStatusClient.getActiveJobs() : new ArrayList<String>();
		} catch (PipelineClientException | ScriptExecException e2) {
			activeList = new ArrayList<>();
		}
		
		for (final PcpCondensedStatusI entity : statusList) {
			log.debug("HCPPipelinesExecManager:  entity - " + entity.getEntityLabel());
			final Date submitStartTime = new Date();
			if (submitsFailed) {
				log.debug("HCPPipelinesExecManager:  submits failed - add to errorMap");
				errorMap.put(entity, failureInfo);
				continue;
			}
			final String pipelineName = getPipelineName(entity);
			if (pipelineName == null) {
				log.error("HCPPipelinesExecManager:  Could not retrieve pipeline name - add to errorMap");
				errorMap.put(entity, "Could not retrieve pipeline name to use for submission");
				continue;
			}
			while(true) {
				log.debug("HCPPipelinesExecManager:  try submission");
				sleep(15*SECONDS);
				//sleep(1*MINUTES);
				updateSubmissionEntitiesAndMap(submissionMap, user, pipelineInfo, false, generateCinabStructure);
				Integer queueCount;
				try {
					queueCount = PipelineStatusClient.getQueuedCount(pipelineInfo.getPipelineName());
				} catch (PipelineClientException | ScriptExecException e1) {
					log.warn("WARNING:  Exception thrown obtaining queuedCount.  Going to sleep and will try again.  "
							+ e1.toString());
					sleep(4*MINUTES);
					continue;
				}
				log.debug("HCPPipelinesExecManager:  queueCount - " + queueCount);
				final boolean excessiveSubmittedOrFailures = excessiveRecentSubmittedOrFailures(submissionMap);
				log.debug("HCPPipelinesExecManager:  excessiveFailures - " + excessiveSubmittedOrFailures);
				if (excessiveSubmittedOrFailures) {
					failureInfo = "Submissions halted due to excessive recent failures or submitted status jobs";
					errorMap.put(entity, failureInfo);
					submitsFailed = true;
					break;
				}
				final boolean timeout = queueWaitTimeout(submitStartTime);
				log.debug("HCPPipelinesExecManager:  timeout - " + timeout);
				if (timeout) {
					failureInfo = "Submissions halted due to timeout";
					errorMap.put(entity, failureInfo);
					submitsFailed = true;
					break;
				}
				//if (queueCount<15 && !excessiveFailures) {
				if (Boolean.parseBoolean(parameters.get("resetCurrentActive")) && listContains(activeList,entity,pipelineInfo)) {
					log.info("HCPPipelinesExecManager:  resetCurrentActive=true and " + entity.getEntityLabel() +
							" is determined to be active.  Adding to submission list for status monitoring.");
					SubmissionStatus submissionStatus = PipelineStatusClient.getJobStatus(entity, pipelineInfo.getPipelineName());
					final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(entity);
					SubmissionInfo submissionInfo = SubmissionInfo.newSubmission();
					if (submissionStatus != null && (submissionStatus.equals(SubmissionStatus.QUEUED) || 
							submissionStatus.equals(SubmissionStatus.RUNNING))) {
						submissionInfo.setSubmissionStatus(submissionStatus);
						if (submissionStatus.equals(SubmissionStatus.QUEUED)) {
							statusEntity.setStatus(PcpConstants.PcpStatus.QUEUED.toString());
						} else if (submissionStatus.equals(SubmissionStatus.RUNNING)) {
							statusEntity.setStatus(PcpConstants.PcpStatus.RUNNING.toString());
						}
					} 
					submissionMap.put(entity, submissionInfo);
					final Date statusTime = new Date();
					statusEntity.setStatusTime(statusTime);
					statusEntity.setStatusInfo("ResetCurrentActive: submission set to " + statusEntity.getStatus() + " at " + new Date() + 
							".  (NODE=" + _nodeUtils.getXnatNode() + ", PIPELINE=" + pipelineInfo.getPipeline() + ", PIPELINE_ARGS=" + pipelineInfo.getPipelineArgs() + ")");
					_statusEntityService.update(statusEntity);
					break;
				}
				if ((queueCount<queuedLimit || Boolean.parseBoolean(parameters.get("overrideQueuedLimit"))) &&
						!excessiveSubmittedOrFailures) {
					if (queueCount>=queuedLimit) {
						log.info("NOTICE: Queued limit has been exceeded (queueCount=" + queueCount + ") but" +
								"overrideQueuedLimit has been specified, so submission will proceed");
					}
					log.debug("HCPPipelinesExecManager:  do submission! - " + entity.getEntityLabel());
					if (PipelineSubmissionClient.submit(entity, getPipelineName(entity), pipelineArgs)) {
						log.info("HCPPipelinesExecManager:  submission - entity=" + entity.getEntityLabel() +
								", pipeline=" + entity.getPipeline() + " - SUCCESSFUL");
						submissionMap.put(entity, SubmissionInfo.newSubmission());
						if (validator instanceof HcpPipelineValidatorI) {
							final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(entity);
							((HcpPipelineValidatorI)validator).invalidatePreviousValidation(statusEntity, user);
							
						}
						break;
					} else {
						log.info("HCPPipelinesExecManager:  submission - entity=" + entity.getEntityLabel() +
								", pipeline=" + entity.getPipeline() + " - FAILED");
						sleep(2*MINUTES);
						boolean submitResult = PipelineSubmissionClient.submit(entity, getPipelineName(entity), pipelineArgs);
						log.info("HCPPipelinesExecManager:  submission - entity=" + entity.getEntityLabel() +
								", pipeline=" + entity.getPipeline() + " - SUCCESS=" + submitResult);
						submissionMap.put(entity, (submitResult) ? SubmissionInfo.newSubmission() : SubmissionInfo.newFailedSubmission());
						if (!submitResult) {
							final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(entity);
							statusEntity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
							final Date statusTime = new Date();
							statusEntity.setStatusTime(statusTime);
							statusEntity.setStatusInfo("Processing submission failed at " + new Date() +
							 		".  (NODE=" + _nodeUtils.getXnatNode() + ", PIPELINE=" + pipelineInfo.getPipeline() + ", PIPELINE_ARGS=" + pipelineInfo.getPipelineArgs() + ")");
							_statusEntityService.update(statusEntity);
							sleep(2*MINUTES);
						}
						break;
					}
				} else if (queueCount>=queuedLimit) {
					log.info("HCPPipelinesExecManager:  postpone submission due to number of queued jobs (queueCount=" +
							queueCount + ") - entity=" + entity.getEntityLabel() + ", pipeline=" + entity.getPipeline());
					sleep(5*MINUTES);
				} 
			}
		}
		if (submitsFailed) {
			log.debug("HCPPipelinesExecManager:  submitsFailed - set status for remaining entities to ERROR");
			for (final Entry<PcpCondensedStatusI, String> entityEntry : errorMap.entrySet()) {
				PcpCondensedStatusI entity = entityEntry.getKey();
				final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(entity);
				if (statusEntity == null) {
					continue;
				}
				statusEntity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
				final Date statusTime = new Date();
				statusEntity.setStatusTime(statusTime);
				statusEntity.setStatusInfo(entityEntry.getValue());
				_statusEntityService.update(statusEntity);
			}
		}
		monitorSubmissions(submissionMap, user, pipelineInfo, generateCinabStructure);
	}
		

	private void monitorSubmissions(Map<PcpCondensedStatusI, SubmissionInfo> submissionMap, UserI user, PipelineInfo pipelineInfo, boolean generateCinabStructure) {
		final Date monitorStartTime = new Date();
		String failureInfo = "";
		log.info("HCPPipelinesExecManager:  MonitorSubmissions");
		int prevSize = 0;
		int zeroCount = 0;
		while(true) {
			if (REQUEST_STOP_MONITORING) {
				if (REQUEST_STOP_MONITORING_ANY_PIPELINE) {
					log.warn("NOTICE:  Stop monitoring request recieved.  Stopping monitoring.");
					REQUEST_STOP_MONITORING = false;
					break;
				} else {
					String pipeline = null;
					if (submissionMap.size()>0) {
						pipeline = submissionMap.keySet().iterator().next().getPipeline();
					}
					if (pipeline != null && REQUEST_STOP_PIPELINE_SET.contains(pipeline)) {
						REQUEST_STOP_PIPELINE_SET.remove(pipeline);
						if (REQUEST_STOP_PIPELINE_SET.size()<1) {
							REQUEST_STOP_MONITORING = false;
						}
						log.warn("NOTICE:  Stop monitoring request recieved for pipeline " + pipeline + 
								".  Stopping monitoring.");
						break;
					}
				}
			}
			final boolean timeout = monitorTimeout(monitorStartTime);
			if (submissionMap.size()>0) {
				prevSize = submissionMap.size();
			}
			if (timeout) {
				failureInfo = "Could not determine outcome:  monitoring of submissions halted due to timeout";
				log.debug("HCPPipelinesExecManager:  Monitoring halted due to timeout.");
				break;
			}
			log.debug("HCPPipelinesExecManager:  MonitorSubmissions - updateSubmissionEntitiesAndMap");
			int activeCount = updateSubmissionEntitiesAndMap(submissionMap, user, pipelineInfo, true, generateCinabStructure);
			if (activeCount<1) {
				if (prevSize>3 && zeroCount<10) {
					log.info("HCPPipelinesExecManager:  MonitorSubmissions - unexpected zero count.  Sleep and try again.");
					zeroCount+=1;
					//sleep(30*SECONDS);
					sleep(5*MINUTES);
					continue;
				} else {
					log.info("HCPPipelinesExecManager:  MonitorSubmissions - no more incomplete submissions - done monitoring");
				}
				return;
			} else {
				zeroCount = 0;
				log.info("HCPPipelinesExecManager:  MonitorSubmissions - " + submissionMap.size() + 
						" incomplete submissions - keep monitoring.");
				
			}
			log.debug("HCPPipelinesExecManager:  MonitorSubmissions - go to sleep");
			//sleep(30*SECONDS);
			sleep(5*MINUTES);
			log.debug("HCPPipelinesExecManager:  MonitorSubmissions - wake up");
		}
		log.debug("HCPPipelinesExecManager:  monitor step failed - set remaining entities to ERROR status");
		for (final Entry<PcpCondensedStatusI, SubmissionInfo> entityEntry : submissionMap.entrySet()) {
			PcpCondensedStatusI entity = entityEntry.getKey();
			final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(entity);
			if (statusEntity == null) {
				continue;
			}
			statusEntity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
			final Date statusTime = new Date();
			statusEntity.setStatusTime(statusTime);
			statusEntity.setStatusInfo(failureInfo);
			_statusEntityService.update(statusEntity);
		}
		return;
	}

	private int updateSubmissionEntitiesAndMap(Map<PcpCondensedStatusI, SubmissionInfo> submissionMap, UserI user, PipelineInfo pipelineInfo, boolean removeCompletedEntitiesFromMap, 
			boolean generateCinabStructure) {
		log.debug("HCPPipelinesExecManager:  update submission entities");
		Iterator<Entry<PcpCondensedStatusI, SubmissionInfo>> iter = submissionMap.entrySet().iterator();
		try {
			List<String> runningList = PipelineStatusClient.getRunningJobs(pipelineInfo.getPipelineName());
			List<String> queuedList = PipelineStatusClient.getQueuedJobs(pipelineInfo.getPipelineName());
			log.debug("HCPPipelinesExecManager:  queuedList " + queuedList);
			log.debug("HCPPipelinesExecManager:  runningList " + runningList);
			final int listSize = queuedList.size() + runningList.size();
			// When our list sizes go to zero from positive list sizes, let's make sure there's not a transient network
			// issue before putting things in error state.
			if (listSize == 0 && _zeroCountdown == -1 && _prevListSize>=4) {
				log.warn("WARNING:  Unexpected zero-length queued/running list size (prevListSize=" + _prevListSize + 
						").  Beginning zeroCountDown before setting setting entity status to ERROR");
				_zeroCountdown = 5;
				return 0;
			} else if (_prevListSize>=4 && _zeroCountdown == -1 && 
					((((float)listSize/_prevListSize)<.5) || (_prevListSize-listSize>=8))) {
				log.warn("WARNING:  Unexpected drop in list size (listSize=" + listSize + 
						", prevListSize=" + _prevListSize + ").  Beginning zeroCountDown before setting setting entity status to ERROR");
				_zeroCountdown = 5;
				return 0;
			} else if (listSize == 0 && _zeroCountdown > 0) {
				log.warn("WARNING:  Zero-length queued/running list size.  Decrementing zeroCountDown (zeroCountdown=" + _zeroCountdown + ").");
				_zeroCountdown -= 1;
				return 0;
			} else if (_prevListSize>=4 && _zeroCountdown > 0 && 
					((((float)listSize/_prevListSize)<.5 || _prevListSize-listSize>=8))) {
				log.warn("WARNING:  Unexpected drop in list size (listSize=" + listSize + 
						", prevListSize=" + _prevListSize + ").  Decrementing zeroCountDown (zeroCountdown=" +
						_zeroCountdown + ").");
				_zeroCountdown -= 1;
				return 0;
			} else if (listSize>0) {
				_zeroCountdown = -1;
			}
			_prevListSize = listSize;
			while (iter.hasNext()) {
				final Entry<PcpCondensedStatusI, SubmissionInfo> entityEntry = iter.next();
				final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(entityEntry.getKey());
				final String entityLabel = statusEntity.getEntityLabel();
				log.debug("HCPPipelinesExecManager:  update submission entities - entity " + entityLabel);
				log.debug("HCPPipelinesExecManager:  current entity status " + statusEntity.getStatus());
				final SubmissionInfo info = entityEntry.getValue();
				SubmissionStatus status = info.getSubmissionStatus();
				log.debug("HCPPipelinesExecManager:  submissionStatus " + status);
				if (!status.equals(SubmissionStatus.QUEUED) && listContains(queuedList, statusEntity, pipelineInfo)) {
					log.info("HCPPipelinesExecManager:  update submission entities - entity=" +
							entityLabel + " - pipeline=" + statusEntity.getPipeline() + " - set status to QUEUED");
					info.setSubmissionStatus(SubmissionStatus.QUEUED);
					statusEntity.setStatus(PcpConstants.PcpStatus.QUEUED.toString());
					final Date statusTime = new Date();
					statusEntity.setStatusTime(statusTime);
					statusEntity.setStatusInfo("Processing indicated as queued at " + statusTime +
					 		".  (NODE=" + _nodeUtils.getXnatNode() + ", PIPELINE=" + pipelineInfo.getPipeline() + ", PIPELINE_ARGS=" + pipelineInfo.getPipelineArgs() + ")");
					_statusEntityService.update(statusEntity);
					continue;
				}
				if (!status.equals(SubmissionStatus.RUNNING) && listContains(runningList, statusEntity, pipelineInfo)) {
					log.info("HCPPipelinesExecManager:  update submission entities - entity=" +
							entityLabel + " - pipeline=" + statusEntity.getPipeline() + " - set status to RUNNING");
					info.setSubmissionStatus(SubmissionStatus.RUNNING);
					statusEntity.setStatus(PcpConstants.PcpStatus.RUNNING.toString());
					final Date statusTime = new Date();
					statusEntity.setStatusTime(statusTime);
					statusEntity.setStatusInfo("Processing indicated as running at " + statusTime +
					 		".  (NODE=" + _nodeUtils.getXnatNode() + ", PIPELINE=" + pipelineInfo.getPipeline() + ", PIPELINE_ARGS=" + pipelineInfo.getPipelineArgs() + ")");
					_statusEntityService.update(statusEntity);
					continue;
				}
				if (
						(!listContains(runningList, statusEntity, pipelineInfo) && !listContains(queuedList, statusEntity, pipelineInfo)) &&
						(!(System.currentTimeMillis()-info.getLastUpdateTime().getTime()<5*MINUTES &&
								info.getSubmissionStatus().equals(SubmissionStatus.SUBMITTED))) &&
						(!(info.getSubmissionStatus().equals(SubmissionStatus.VALIDATED) ||
								info.getSubmissionStatus().equals(SubmissionStatus.ERROR)))
					) {
						
					PipelineValidatorI validator = getPipelineValidator(statusEntity);
					validator.validate(statusEntity, user);
					if (statusEntity.getValidated()) {
						log.info("HCPPipelinesExecManager:  update submission entities - entity=" +
								entityLabel + " - pipeline=" + statusEntity.getPipeline() + " - set status to VALIDATED");
						info.setSubmissionStatus(SubmissionStatus.VALIDATED);
						statusEntity.setStatus(PcpConstants.PcpStatus.COMPLETE.toString());
						final Date statusTime = new Date();
						statusEntity.setStatusTime(statusTime);
						statusEntity.setStatusInfo("Processing complete and entity is reported as validated at " + statusTime +
					 		".  (NODE=" + _nodeUtils.getXnatNode() + ", PIPELINE=" + pipelineInfo.getPipeline() + ", PIPELINE_ARGS=" + pipelineInfo.getPipelineArgs() + ")");
						
						if (generateCinabStructure) {
							log.info("CinaB generation requested.  Requesting CinaB structure for " + entityLabel + ".");
							_cinabUtils.generateCinabForEntity(statusEntity.getProject(), statusEntity.getEntityLabel(), user);
						}
						
					} else if (_zeroCountdown<=0) {
						log.info("HCPPipelinesExecManager:  update submission entities - entity=" +
								entityLabel + " - pipeline=" + statusEntity.getPipeline() + " - set status to ERROR");
						info.setSubmissionStatus(SubmissionStatus.ERROR);
						statusEntity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
						final Date statusTime = new Date();
						statusEntity.setStatusTime(statusTime);
						statusEntity.setStatusInfo("Processing complete but entity failed validation " + statusTime +
					 		".  (NODE=" + _nodeUtils.getXnatNode() + ", PIPELINE=" + pipelineInfo.getPipeline() + ", PIPELINE_ARGS=" + pipelineInfo.getPipelineArgs() + ")");
					}
					_statusEntityService.update(statusEntity);
					if (removeCompletedEntitiesFromMap && (_zeroCountdown<=0 || statusEntity.getValidated())) {
						log.info("HCPPipelinesExecManager:  update submission entities - entity=" +
								entityLabel + " - pipeline=" + statusEntity.getPipeline() + " - completed, remove");
						iter.remove();
					}
					continue;
				}
				if (info.getSubmissionStatus().equals(SubmissionStatus.VALIDATED) ||
							info.getSubmissionStatus().equals(SubmissionStatus.ERROR)) {
					if (removeCompletedEntitiesFromMap) {
						log.info("HCPPipelinesExecManager:  update submission entities - entity=" +
								entityLabel + " - pipeline=" + statusEntity.getPipeline() + " - completed, remove");
						iter.remove();
					}
				}
			}
			return submissionMap.size();
			
		} catch (PipelineClientException | ScriptExecException e) {
			log.error("ERROR:  Couldn't retrieve pipelines queuing status",e);
			sleep(2*MINUTES);
			return submissionMap.size();
		}
	}

	private boolean listContains(List<String> statusList, PcpCondensedStatusI statusEntity, PipelineInfo pipelineInfo) {
		for (final String jobName : statusList) {
			final String entityLabel = statusEntity.getEntityLabel();
			if (!jobName.contains("." + pipelineInfo.getPipelineName())) {
				continue;
			}
			if (jobName.startsWith(entityLabel + ".") || 
			    jobName.startsWith(entityLabel + "_" + statusEntity.getSubGroup())) {
				return true;
			}
		}
		return false;
	}

	private boolean queueWaitTimeout(Date submitStartTime) {
		// TODO:  Should this be configurable?
		return ((System.currentTimeMillis()-submitStartTime.getTime())>(10*HOURS));
	}

	private boolean monitorTimeout(Date monitorStartTime) {
		// TODO:  Should this be configurable?
		return ((System.currentTimeMillis()-monitorStartTime.getTime())>(10*DAYS));
	}

	private PipelineValidatorI getPipelineValidator(PcpCondensedStatusI entity) {
		try {
			final PipelineValidatorI validator = _pcpUtils.getComponentSet(entity).getValidator();
			return validator;
		} catch (PcpComponentSetException e) {
			return null;
		}
	}

	private String getPipelineName(PcpCondensedStatusI entity) {
		final PipelineValidatorI validator = getPipelineValidator(entity);
		if (validator == null || !(validator instanceof HcpPipelineValidatorI)) {
			return null;
		}
		return ((HcpPipelineValidatorI)validator).getPipelineName();
	}


	private boolean excessiveRecentSubmittedOrFailures(Map<PcpCondensedStatusI, SubmissionInfo> submissionMap) {
		log.debug("HCPPipelinesExecManager: Check for excessive recent failures");
		final Date currentTime = new Date();
		int errorCount = 0;
		for (Entry<PcpCondensedStatusI, SubmissionInfo> submission : submissionMap.entrySet()) {
			SubmissionInfo submissionInfo = submission.getValue();
			if ((currentTime.getTime()-submissionInfo.getLastUpdateTime().getTime())>(30*MINUTES)) {
				continue;
			}
			if (submissionInfo.getSubmissionStatus().equals(SubmissionStatus.ERROR) ||
					submissionInfo.getSubmissionStatus().equals(SubmissionStatus.SUBMITTED)) {
				errorCount++;
				if (errorCount>10) {
					log.info("HCPPipelinesExecManager: ExcessiveSubmittedOrFailures=TRUE!");
					return true;
				}
			}
		}
		log.debug("HCPPipelinesExecManager: ExcessiveFailures=FALSE.  ErrorCount=" + errorCount);
		return false;
	}
	
	private void sleep(long duration) {
		try {
			Thread.sleep(duration);
		} catch (InterruptedException e) {
			// Do nothing
		}
	}

}

