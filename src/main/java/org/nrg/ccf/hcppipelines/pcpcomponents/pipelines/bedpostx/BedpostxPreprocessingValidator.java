package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.bedpostx;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@PipelineValidator
public class BedpostxPreprocessingValidator extends AbstractHcpPipelinesValidator {
	
	public BedpostxPreprocessingValidator() {
		super();
		EXPECTED_FILE_URL = PipelineConstants.EXPECTEDFILES_FILE_URL + 
			"/BedpostxPreprocessing.txt";
	}

	@Override
	public String getPipelineName() {
		return PipelineConstants.BEDPOSTX_PIPELINE_NAME;
	}

	@Override
	public String getPipelineResourceLabel(PcpStatusEntity entity) {
		return PipelineConstants.BEDPOSTX_RESOURCE_LABEL;
	}
	
	@Override
	protected boolean isProcessingComplete(final PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource) {
		final XnatResourcecatalog diffusionPreprocResource = ResourceUtils.getResource(session, PipelineConstants.DIFFUSION_PREPROC_RESOURCE_LABEL);
		if (diffusionPreprocResource == null) {
			return outcome(entity, "Could not open prerequisite (DiffusionPreprocessing) resource", false);
		}
		checkCompletionFileModTime(entity, resource, diffusionPreprocResource);
		return checkExpectedFiles(entity, session, resource);
	}

}
