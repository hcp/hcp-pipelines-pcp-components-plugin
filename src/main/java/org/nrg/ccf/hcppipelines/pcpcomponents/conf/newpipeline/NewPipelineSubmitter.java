package org.nrg.ccf.hcppipelines.pcpcomponents.conf.newpipeline;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xft.security.UserI;

import com.google.common.collect.ImmutableList;

@PipelineSubmitter
public class NewPipelineSubmitter implements PipelineSubmitterI {

	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		return ImmutableList.of();
	}

	@Override
	public boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator,
			Map<String, String> parameters, UserI user) throws PcpSubmitException {
		entity.setStatus(PcpConstants.PcpStatus.ERROR);
		entity.setStatusInfo("Submission functionality is not yet implemented");
		entity.setStatusTime(new Date());
		entityService.update(entity);
		return false;
	}

}
