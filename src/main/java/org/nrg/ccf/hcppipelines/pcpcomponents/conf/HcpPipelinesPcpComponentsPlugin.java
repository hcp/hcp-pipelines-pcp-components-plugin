package org.nrg.ccf.hcppipelines.pcpcomponents.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "hcpPipelinesPcpComponentsPlugin",
			name = "HCP Pipelines PCP Components Plugin",
			log4jPropertiesFile = "/META-INF/resources/hcpPipelinesLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.hcppipelines.pcpcomponents.conf",
		"org.nrg.ccf.hcppipelines.pcpcomponents.components",
		"org.nrg.ccf.hcppipelines.pcpcomponents.xapi"
	})
public class HcpPipelinesPcpComponentsPlugin {
	
	public static Logger logger = Logger.getLogger(HcpPipelinesPcpComponentsPlugin.class);

	public HcpPipelinesPcpComponentsPlugin() {
		logger.info("Configuring the HCP Pipelines PCP Components Plugin.");
	}
	
}
