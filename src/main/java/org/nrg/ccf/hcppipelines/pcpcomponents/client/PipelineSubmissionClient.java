package org.nrg.ccf.hcppipelines.pcpcomponents.client;

import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.common.utilities.components.NodeUtils;
import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractPipelineClient;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.xdat.XDAT;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PipelineSubmissionClient extends AbstractPipelineClient {
	
	final static String PIPELINE_SUBMITTER_COMMAND = System.getProperty("user.home") + "/bin/pipelineSubmitter";
	private static NodeUtils _nodeUtils; 
	
	private static NodeUtils getNodeUtils() {
		if (_nodeUtils == null) {
			_nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
		}
		return _nodeUtils;
	}

	public static boolean submit(PcpCondensedStatusI entity, String pipelineName, String pipelineArgs) {
		final String entityLabel = entity.getEntityLabel();
		// NOTE:  Not always a subject but using HCPpipelinesXnatPbsJobs naming convention
		final String subject_id = entityLabel.replaceFirst("_.*$", "");
		final String classifier = entityLabel.replaceFirst("^[^_]*_", "");
		final String subgroup = entity.getSubGroup();
		final String extra = (subgroup.equalsIgnoreCase("ALL")) ? "all" : subgroup;
		final String project = entity.getProject();
		return submit(subject_id, classifier, extra, project, pipelineName, pipelineArgs);
	}
		
	public static boolean submit(String subject_id, String classifier, String extra, String project, String pipelineName, String pipelineArgs) {
		
		try {
			 final ScriptResult result = submitPipeline(subject_id, classifier, extra, project, pipelineName, pipelineArgs);
			 final List<String> resultList = result.getListResult();
			 for (final String resultStr : resultList) {
				 if (resultStr.equalsIgnoreCase("SUBMIT_OK")) {
					 log.info("ScriptExecution reported to be successful");
					 log.info("      STATUS:  " + result.getStatus());
					 log.info("      STDOUT:  " + result.getStdout().toString());
					 log.info("      STDERR:  " + result.getStderr().toString());
					 return true;
				 }
			 }
			 log.error("ScriptExecution not successful");
			 log.error("      STATUS:  " + result.getStatus());
			 log.error("      STDOUT:  " + result.getStdout().toString());
			 log.error("      STDERR:  " + result.getStderr().toString());
			 return false;
		} catch (Exception e) {
			return false;
		} 
	}
		
	public static ScriptResult submitPipeline(String subject_id, String classifier, String extra, String project, String pipelineName) throws Exception {
		
		return submitPipeline(subject_id, classifier, extra, project, pipelineName, null);
		
	}
		
	public static ScriptResult submitPipeline(String subject_id, String classifier, String extra, String project, String pipelineName, String paramInfo) throws Exception {
		
		final StringBuilder cmdSB = new StringBuilder(PIPELINE_SUBMITTER_COMMAND)
		.append(" ").append(getNodeUtils().getXnatNode())
		.append(" ").append(pipelineName);
		if (paramInfo != null && paramInfo.length()>0) {
			cmdSB.append(":").append(paramInfo);
		}
		cmdSB.append(" ").append(project)
		.append(":").append(subject_id)
		.append(":").append(classifier)
		.append(":") .append(extra);
		try {
			 final String commandStr = cmdSB.toString();
			 log.info("Executing submit command:  " + commandStr);
			 return executeCommand(commandStr);
		} catch (Exception e) {
			log.error("HCP Pipelines submission error (Exception thrown:");
			log.error(ExceptionUtils.getFullStackTrace(e));
			throw e;
		} 
		
	}

}
