package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.structuralpreprocessing;

import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractNoFieldmapSupportPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class StructuralPreprocessingValidator extends AbstractNoFieldmapSupportPipelinesValidator {
	
	// Subclasses may need to override the ones in PipelineConstants
	protected String T1_REGEX;
	protected String T2_REGEX;

	@Override
	public List<String> getConfigurationYaml() {
		List<String> configYaml = super.getConfigurationYaml();
		final StringBuilder sb = new StringBuilder()
				.append("t1Regex:\n")
				.append("    id: " + _className + "-t1-regex\n")
				.append("    name: " + _className + "-t1-regex\n")
				.append("    kind: panel.input.text\n")
				.append("    placeholder: " + PipelineConstants.T1_REGEX + "\n")
				.append("    label: Regular expression for matching T1 unproc resources\n")
				.append("t2Regex:\n")
				.append("    id: " + _className + "-t2-regex\n")
				.append("    name: " + _className + "-t2-regex\n")
				.append("    kind: panel.input.text\n")
				.append("    placeholder: " + PipelineConstants.T2_REGEX + "\n")
				.append("    label: Regular expression for matching T2 unproc resources\n")
				;
		configYaml.add(sb.toString());
		return configYaml;
	}

	public StructuralPreprocessingValidator() {
		super();
		EXPECTED_FILE_URL = PipelineConstants.EXPECTEDFILES_FILE_URL +
			"/StructuralPreprocessingWithFieldmaps.txt";
		NOFIELDMAP_EXPECTED_FILE_URL = PipelineConstants.EXPECTEDFILES_FILE_URL + 
			"/StructuralPreprocessing.txt";
	}

	@Override
	public String getPipelineName() {
		return PipelineConstants.STRUC_PREPROC_PIPELINE_NAME;
	}

	@Override
	public String getPipelineResourceLabel(PcpStatusEntity entity) {
		return PipelineConstants.STRUC_PREPROC_RESOURCE_LABEL;
	}
	
	@Override
	protected boolean isProcessingComplete(final PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource) {
		
		try {
			T1_REGEX = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
							this.getClass().getName(), "-t1-regex", false).toString();
			T2_REGEX = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
						this.getClass().getName(), "-t2-regex", false).toString();
		} catch (PcpAutomationScriptExecutionException e) {
			log.warn("Exception thrown obtaining config values (T1_REGEX, T2_REGEX");
		}
		if (T1_REGEX == null || T1_REGEX.isEmpty()) {
			T1_REGEX = PipelineConstants.T1_REGEX;
		}
		if (T2_REGEX == null || T2_REGEX.isEmpty()) {
			T2_REGEX = PipelineConstants.T2_REGEX;
		}
		
		final XnatResourcecatalog t1unproc = ResourceUtils.getSingleResourceMatchingRegex(session, T1_REGEX);
		final XnatResourcecatalog t2unproc = ResourceUtils.getSingleResourceMatchingRegex(session, T2_REGEX);
		if (t1unproc == null || t2unproc == null) {
			return outcome(entity, "Could not open prerequisite (unproc) resources", false);
		}
		final boolean hasFieldmaps = containsTopLevelFieldmaps(t1unproc, t2unproc);
		/*
		// MRH:  Commenting this out because it's not a good check for Structural preprocessing.  Catalog files
		// modification times can be updated regularly.  If anything, we need to check the mod time of the actual NIFTI files.
		// Let's just disable this check for now.
		if (! ((t1unproc.getCatalogFile(CommonConstants.ROOT_PATH).lastModified()<
				 resource.getCatalogFile(CommonConstants.ROOT_PATH).lastModified()) &&
		       (t2unproc.getCatalogFile(CommonConstants.ROOT_PATH).lastModified()<
				 resource.getCatalogFile(CommonConstants.ROOT_PATH).lastModified())
				 )) {
			// We want to be able to override this issue (set issue to no, so we won't flag this again 
			// if we've already determined this to be a non-issue
			if (!(entity.getIssuesInfo().contains("older than prerequisite"))) {
				entity.setIssues(true);
				entity.setIssuesTime(new Date());
				entity.setIssuesInfo("Processing resource completion marker file is older than prerequisite completion marker file.");
			}
		}
		*/
		return checkExpectedFiles(entity, session, resource, hasFieldmaps);
	}

}
