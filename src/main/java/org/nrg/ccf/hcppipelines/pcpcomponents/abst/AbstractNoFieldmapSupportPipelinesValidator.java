package org.nrg.ccf.hcppipelines.pcpcomponents.abst;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
public abstract class AbstractNoFieldmapSupportPipelinesValidator extends AbstractHcpPipelinesValidator {
	
	public String NOFIELDMAP_EXPECTED_FILE_URL;
	
	protected boolean checkExpectedFiles(PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource,
			boolean hasFieldmaps) {
		if (skipFileRechecks(entity) && entity.getValidated()) {
			return outcome(entity, "NOTE:  Expected file check was skipped for this run per configuration.", true);
		}
		final String resourcePath = ResourceUtils.getResourcePath(resource);
		final List<String> resourceFiles = ResourceUtils.getRelativeFilePaths(resource);
		//log.error("resourceFiles = \n\n" + StringUtils.join(resourceFiles,"\n"));
		final List<String> expectedFiles;
		try {
			expectedFiles = getExpectedFiles(session.getLabel(), hasFieldmaps);
			//log.error("expectedFiles = <br><br>" + StringUtils.join(expectedFiles,"\n"));
		} catch (IOException e) {
			return outcome(entity, "IOException:  Could not return expected files from EXPECTED_FILE_URL (" +
					EXPECTED_FILE_URL + ")<br><br>" + ExceptionUtils.getStackTrace(e), false);
		}
		final StringBuilder sb = new StringBuilder();
		for (final String expected : expectedFiles) {
			if (!resourceFiles.contains(expected)) {
				// The "expected" files contain directories while the resource file lists do not.  We'll check
				// if the missing file is a directory here.
				final File checkFile = new File(resourcePath + File.separator + expected);
				if (checkFile.exists()) {
					continue;
				}
				sb.append(" MissingFile:  " + expected + "<br>\n");
			}
		}
		if (sb.length()>0) {
			return outcome(entity, "Missing one or more expectedFiles (EXPECTED_FILE_URL=" + EXPECTED_FILE_URL +
					"):  <br><br>\n\n" + sb.toString(), false);
		}
		return true;
	}

	protected boolean containsTopLevelFieldmaps(XnatResourcecatalog t1unproc, XnatResourcecatalog t2unproc) {
		final List<String> filePaths = new ArrayList<>();
		filePaths.addAll(ResourceUtils.getRelativeFilePaths(t1unproc));
		filePaths.addAll(ResourceUtils.getRelativeFilePaths(t2unproc));
		for (final String fl : filePaths) {
			if (fl.contains("OTHER_FILES")) {
				continue;
			}
			if (fl.toLowerCase().contains("spinecho") || fl.toLowerCase().contains("fieldmap")) {
				return true;
			}
		}
		return false;
	}

	protected List<String> getExpectedFiles(final String expLabel, final boolean hasFieldmaps) throws IOException {
		if (_expectedFileCache == null) {
			_expectedFileCache = new ArrayList<>();
			final URL expUrl = new URL((hasFieldmaps) ? EXPECTED_FILE_URL : NOFIELDMAP_EXPECTED_FILE_URL);
			final URLConnection conn = expUrl.openConnection();
			InputStream is = conn.getInputStream();
	        InputStreamReader isr = new InputStreamReader(is);
	        BufferedReader br = new BufferedReader(isr);
	        String inputLine;
	        while ((inputLine = br.readLine()) != null) {
	        	if (inputLine.trim().startsWith("#")) {
	        		continue;
	        	}
	            _expectedFileCache.add(inputLine);
	        }
	        br.close();
        
		}
		final List<String> returnList = new ArrayList<>();
        for (final String inputLine : _expectedFileCache) {
            String expectedFile = inputLine.replaceAll("\\{subjectid\\}", expLabel).replaceAll(" ",File.separator);
            if (!expectedFile.startsWith(expLabel + File.separator) ) {
            	expectedFile = expLabel + File.separator + expectedFile;
            }
        }
        return returnList;
	}

}
