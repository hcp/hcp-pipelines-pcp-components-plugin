package org.nrg.ccf.hcppipelines.pcpcomponents.inter;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xft.security.UserI;

public interface HcpPipelineValidatorI extends PipelineValidatorI {
	
	public String getPipelineName();
	
	public void invalidatePreviousValidation(PcpStatusEntity entity, UserI user);

}
