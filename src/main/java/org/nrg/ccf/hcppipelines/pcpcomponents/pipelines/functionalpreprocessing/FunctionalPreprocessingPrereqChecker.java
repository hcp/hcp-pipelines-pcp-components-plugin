package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.functionalpreprocessing;

import java.util.Arrays;

import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;

@PipelinePrereqChecker
public class FunctionalPreprocessingPrereqChecker extends AbstractHcpPipelinesPrereqChecker {

	public FunctionalPreprocessingPrereqChecker() {
		super();
		PREREQ_PIPELINE_NAME = PipelineConstants.STRUC_PREPROC_PIPELINE_NAME;
		PREREQ_PIPELINE_PACKAGES = 
				Arrays.asList(new String[] { 
						"org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.structuralpreprocessing" 
				});
	}
	
}
