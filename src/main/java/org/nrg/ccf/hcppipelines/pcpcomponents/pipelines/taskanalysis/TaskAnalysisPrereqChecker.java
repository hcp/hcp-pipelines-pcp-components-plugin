package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.taskanalysis;

import java.util.Arrays;

import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;

@PipelinePrereqChecker
public class TaskAnalysisPrereqChecker extends AbstractHcpPipelinesPrereqChecker {

	public TaskAnalysisPrereqChecker() {
		super();
		PREREQ_PIPELINE_NAME = PipelineConstants.MULTIRUNICAFIX_PIPELINE_NAME;
		PREREQ_PIPELINE_PACKAGES = 
				Arrays.asList(new String[] { 
						"org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.multirunicafix" 
				});
	}
	
}
