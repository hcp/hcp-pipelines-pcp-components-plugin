package org.nrg.ccf.hcppipelines.pcpcomponents.exception;

public class PipelineClientException extends Exception {
	
	private static final long serialVersionUID = -8633439954053632737L;

	public PipelineClientException() {
		super();
	}

	public PipelineClientException(String message) {
		super(message);
	}

	public PipelineClientException(Throwable cause) {
		super(cause);
	}

	public PipelineClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public PipelineClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
