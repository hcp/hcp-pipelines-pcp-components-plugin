package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.resourcelocker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelinePrereqChecker
public class ResourceLockerPrereqChecker implements PipelinePrereqCheckerI, ConfigurableComponentI {
	
	private final Map<String,List<String>> _configCache = new HashMap<>();
	private final Map<String,Map<String,List<String>>> _modCache = new HashMap<>();
	private List<String> _configYaml = null;
	protected final String _className = this.getClass().getName().replace(".", "");
	protected final String _infoStrRequiresLock = "Resource requires locking per configuration";
	protected final String _infoStrNoLock = "This resource has not been indicated by configuration for locking";
	
	@Override
	public List<String> getConfigurationYaml() {
		if (_configYaml != null) {
			return _configYaml;
		}
		_configYaml = new ArrayList<>();
		final StringBuilder sb = new StringBuilder();
		sb.append(_className)
		.append("LockRequiredEntities:\n")
		.append("    id: " + _className + "-lock-required-entities\n")
		.append("    name: " + _className + "-lock-required-entities\n")
		.append("    kind: panel.textarea\n")
		.append("    label: Entities To Be Locked:\n")
		.append("    description: List of entities/resources that should be locked (set to read-only). "
				+ "These can be listed as [entity] to lock all resources and [entity]:resource,resource2 " + 
				"if only certain pipeline outputs should be locked for a subject.  If instead you wish to " +
				"indicate which resources NOT to lock (an exclude list), please indicate each resource with a '-' "
				+ "preceeding the name (e.g. [SESSIONID]:-MsmAll_proc:-MultiRunIcaFix_proc).  It's best not "
				+ "to mix inclusions and exclusions on the same session, but within a configuration, some "
				+ "sessions may have an include list, while others may have an exclude list");
		_configYaml.add(sb.toString());
		return _configYaml;
	}
	
	protected List<String> getConfiguredValues(String project, String pipeline) throws PcpAutomationScriptExecutionException {
		final String configValue = PcpComponentUtils.getConfigValue(project, pipeline, this.getClass().getName(),
				"-lock-required-entities", false).toString();
		if (!_configCache.containsKey(project)) {
			final List<String> returnLst = new ArrayList<>();
			final String configArr[] = configValue.split("\\r?\\n");
			for (String configV : configArr) {
				if (configV == null) {
					continue;
				}
				configV = configV.trim();
				if (configV.length()>0 && !returnLst.contains(configV)) {
					returnLst.add(configV);
				}
			}
			_configCache.put(project, returnLst);
		}
		return _configCache.get(project);
	}
	
	protected Map<String,List<String>> getModCacheValues(String project, String pipeline) throws PcpAutomationScriptExecutionException {
		if (!_modCache.containsKey(project)) {
			final Map<String,List<String>> returnMap = new HashMap<>();
			final List<String> toBeLocked = getConfiguredValues(project, pipeline);
			for (final String config : toBeLocked) {
				final String[] configSplit = config.split(":");
				final String configEntity = configSplit[0].trim();
				final String[] resourceSplit;
				if (configSplit.length>1) {
					resourceSplit = configSplit[1].split(",");
				} else {
					resourceSplit = new String[] {};
				}
				if (resourceSplit.length==0) {
					returnMap.put(configEntity, new ArrayList<>());
				} else {
					final List<String> resourceList = new ArrayList<>();
					resourceList.addAll(Arrays.asList(resourceSplit));
					returnMap.put(configEntity, resourceList);
				}
			}
			_modCache.put(project, returnMap);
		}
		return _modCache.get(project);
	}

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		try {
			Map<String,List<String>> configMap = getModCacheValues(statusEntity.getProject(),statusEntity.getPipeline());
			if (configMap.containsKey(statusEntity.getEntityLabel())) {
				final List<String> resourceList = configMap.get(statusEntity.getEntityLabel());
				if (resourceList.size()==0) {
					statusEntity.setPrereqs(true);
					statusEntity.setPrereqsTime(new Date());
					statusEntity.setPrereqsInfo(_infoStrRequiresLock);
					return;
				} else {
					final List<String> includeList = new ArrayList<>();
					final List<String> excludeList = new ArrayList<>();
					for (String resource : resourceList) {
						if (resource.startsWith("-")) {
							excludeList.add(resource.substring(1));
						} else {
							includeList.add(resource);
						}
					}
					boolean needDefaced = true;
					final String subgroup = statusEntity.getSubGroup();
					if (excludeList.size()>0 && excludeList.contains(subgroup)) {
						needDefaced = false;
					} else if (includeList.size()>0 && !includeList.contains(subgroup)) {
						needDefaced = false;
					}
					if (needDefaced) {
						statusEntity.setPrereqs(true);
						statusEntity.setPrereqsTime(new Date());
						statusEntity.setPrereqsInfo(_infoStrRequiresLock);
						return;
					}
				}
			}
			statusEntity.setPrereqs(false);
			statusEntity.setPrereqsTime(new Date());
			statusEntity.setPrereqsInfo(_infoStrNoLock);
		} catch (Exception e) {
			log.warn(e.toString());
		}
	}

}
