package org.nrg.ccf.hcppipelines.pcpcomponents.abst;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.inter.HcpPipelineValidatorI;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.archive.CatalogService;

import lombok.extern.slf4j.Slf4j;

//import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractHcpPipelinesValidator implements HcpPipelineValidatorI, ConfigurableComponentI {
	
	
	protected List<String> _expectedFileCache;
	protected String EXPECTED_FILE_URL;
	protected final String _className = this.getClass().getName().replace(".", "");
	
	public abstract String getPipelineResourceLabel(PcpStatusEntity entity);
	private Boolean _treatTimestampCheckAsIssue = null;
	private Boolean _skipFileRechecks = null;
	CatalogService _catService;


	protected abstract boolean isProcessingComplete(PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource);
	

	@Override
	public List<String> getConfigurationYaml() {
		List<String> configYaml = new ArrayList<>();
		final StringBuilder sb = new StringBuilder()
				.append("timestampIssueOrWarning:\n")
				.append("    id: " + _className + "-timestamp-issue-or-warning\n")
				.append("    name: " + _className + "-timestamp-issue-or-warning\n")
				.append("    kind: panel.select.single\n")
				.append("    label: Treat timestamp check as issue?\n")
				.append("    description: If yes, prereq timestamp check failure will be treated as an issue. " +
								"Otherwise it will just be a warning.\n")
				.append("    value: 'false'\n")
				.append("    options:\n")
				.append("        'true': 'Yes'\n")
				.append("        'false': 'No'\n")
				.append("skipFileRechecks:\n")
				.append("    id: " + _className + "-skip-file-rechecks\n")
				.append("    name: " + _className + "-skip-file-rechecks\n")
				.append("    kind: panel.select.single\n")
				.append("    label: Skip re-checking of file existence after initial check?\n")
				.append("    description: If yes, reruns of validation will not recheck that each file still exists in the archive. " +
								"Checks will be limited to completion marker file checks and other less resource intensive checks.  The " +
						        "file list will be checked anytime validated=False.\n")
				.append("    value: 'true'\n")
				.append("    options:\n")
				.append("        'true': 'Yes'\n")
				.append("        'false': 'No'\n")
				/*
				.append("    kind: panel.input.switchbox\n")
				.append("    label: Treat timestamp check as issue?\n")
				.append("    description: If yes, prereq timestamp check failure will be treated as an issue. " +
								"Otherwise it will just be a warning.\n")
				.append("    value: false\n")
				.append("    onText: Yes\n")
				.append("    offText: No\n");
				*/
				;
		configYaml.add(sb.toString());
		return configYaml;
	}
	
	protected boolean treatTimestampCheckAsIssue(PcpStatusEntity entity) {
		if (_treatTimestampCheckAsIssue == null) {
			try {
				final String configValue = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
					this.getClass().getName(), "-timestamp-issue-or-warning", false).toString();
				_treatTimestampCheckAsIssue = (configValue!=null) ? Boolean.valueOf(configValue) : false;
			} catch (Exception e) {
				_treatTimestampCheckAsIssue = false;
			}
		}
		return _treatTimestampCheckAsIssue;
	}
	
	protected boolean skipFileRechecks(PcpStatusEntity entity) {
		if (_skipFileRechecks == null) {
			try {
				final String configValue = PcpComponentUtils.getConfigValue(entity.getProject(), entity.getPipeline(), 
					this.getClass().getName(), "-skip-file-rechecks", false).toString();
				_skipFileRechecks = (configValue!=null) ? Boolean.valueOf(configValue) : false;
			} catch (Exception e) {
				_skipFileRechecks = true;
			}
		}
		return _skipFileRechecks;
	}
	
	@Override
	public void invalidatePreviousValidation(PcpStatusEntity entity, UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(entity.getEntityId(), user, false);
		final XnatAbstractresourceI absResource = ResourceUtils.getResource(session, getPipelineResourceLabel(entity)); 
		if (absResource == null) { 
			return;
		}
		final XnatResourcecatalog resource = (absResource instanceof XnatResourcecatalog) ? (XnatResourcecatalog) absResource : null;
		if (resource == null) {
			return;
		}
		final File checkFile = getCompletionMarkerFile(resource);
		if (checkFile == null) {
			return;
		}
		final File invalidatedFile = new File(checkFile.getPath() + ".invalidated");
		if (!invalidatedFile.exists()) {
			checkFile.renameTo(invalidatedFile);
			try {
				log.error("Here it is: " + resource.getUri());
				log.error("Here it is (base): " + resource.getBaseURI());
				log.error("Here it is (sb): " + ResourceUtils.getResourceSb(resource));
				getCatalogService().refreshResourceCatalog(user, ResourceUtils.getResourceSb(resource), CatalogService.Operation.ALL);
				log.error("DONE!!!");
			} catch (ServerException | ClientException e) {
				// Undo rename
				log.warn("WARNING:  Couldn't invalidate previous completion file");
				log.warn(resource.getUri());
				log.warn(ExceptionUtils.getFullStackTrace(e));
				invalidatedFile.renameTo(checkFile);
			}
		}
	}
	
	private CatalogService getCatalogService() {
		if (_catService == null) {
			_catService = XDAT.getContextService().getBean(CatalogService.class);
		}
		return _catService;
	}
	
	private String fileInfoString(final File f) {
		return fileInfoString(f, false);
	}
	
	private String fileInfoString(final File f, final Boolean isPrereqFile) {
		if (f == null) {
			return "";
		}
        BasicFileAttributes attr;
		try {
			attr = Files.readAttributes(Paths.get(f.getPath()), BasicFileAttributes.class);
			return (" (" + ((isPrereqFile) ? "prereqFile=" : "checkFile=") + f.getName() + ", modTime=" + attr.lastModifiedTime() + ") ");
		} catch (IOException e) {
			return "";
		}
	}

	@Override
	public void validate(PcpStatusEntity entity, UserI user) {
		
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(entity.getEntityId(), user, false);
		final XnatAbstractresourceI absResource = ResourceUtils.getResource(session, getPipelineResourceLabel(entity)); 
		final XnatResourcecatalog resource = (absResource instanceof XnatResourcecatalog) ? (XnatResourcecatalog) absResource : null;
		if (resource == null) {
			outcome(entity, getPipelineResourceLabel(entity) + " resource does not exist", false);
			return;
		}
		final File checkFile = getCompletionMarkerFile(resource);
		if (!checkIsProcessingMarkedComplete(entity, resource, checkFile)) {
			return;
		}
		entity.setValidatedInfo("");
		if (entity.getIssues()) {
			entity.setIssues(false);
			entity.setIssuesTime(new Date());
			entity.setIssuesInfo("");
		}
		if (!isProcessingComplete(entity, session, resource)) {
			return;
		}
		entity.setValidated(true);
		entity.setValidatedTime(new Date());
		entity.setValidatedInfo("Processing is complete and verified " + fileInfoString(checkFile) + ".  " + entity.getValidatedInfo());
	}
	
	protected void checkCompletionFileModTime(PcpStatusEntity entity, XnatResourcecatalog entityResource, List<XnatResourcecatalog> checkResources) {
		for (XnatResourcecatalog prereqCat : checkResources) {
			final File checkFile = getCompletionMarkerFile(prereqCat);
			final File entityFile = getCompletionMarkerFile(entityResource);
			if (checkFile == null || entityFile == null) {
				// Let's not worry about missing completion files here.  That will be caught elsewhere 
				// in validation. 
				continue;
			}
			if (! (checkFile.lastModified()<
					 entityFile.lastModified())
					) {
				// We want to be able to override this issue (set issue to no, so we won't flag this again 
				// if we've already determined this to be a non-issue
				if (treatTimestampCheckAsIssue(entity)) {
					if (!(entity.getIssuesInfo().contains("older than one or more prerequisite"))) {
						entity.setIssues(true);
						entity.setIssuesTime(new Date());
						entity.setIssuesInfo("Processing resource completion marker file is older than one or more prerequisite completion marker files " 
								+ fileInfoString(checkFile) + " " + fileInfoString(checkFile, true));
					} else {
						entity.setValidatedInfo("WARNING:  Processing resource completion marker file is older than prerequisite completion marker file(s). "
								+ fileInfoString(checkFile) + " " + fileInfoString(checkFile, true));
					}
				}
			}
		}
	}

	protected void checkCompletionFileModTime(PcpStatusEntity entity, XnatResourcecatalog entityResource, XnatResourcecatalog checkResource) {
		final File checkFile = getCompletionMarkerFile(checkResource);
		final File entityFile = getCompletionMarkerFile(entityResource);
		if (checkFile == null || entityFile == null) {
			// Let's not worry about missing completion files here.  That will be caught elsewhere 
			// in validation. 
			return;
		}
		if (! ((checkFile.lastModified()<
				 entityFile.lastModified())
				 )) {
			// We want to be able to override this issue (set issue to no, so we won't flag this again 
			// if we've already determined this to be a non-issue
			if (treatTimestampCheckAsIssue(entity)) {
				if (!(entity.getIssuesInfo().contains("older than prerequisite"))) {
					entity.setIssues(true);
					entity.setIssuesTime(new Date());
					entity.setIssuesInfo("Processing resource completion marker file is older than prerequisite completion marker file."
								+ fileInfoString(checkFile) + " " + fileInfoString(checkFile, true));
				}
			} else {
				entity.setValidatedInfo("WARNING:  Processing resource completion marker file is older than prerequisite completion marker file. " 
								+ fileInfoString(checkFile) + " " + fileInfoString(checkFile, true));
			}
		}
	}
	
	protected boolean checkExpectedFiles(PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource) {
		if (skipFileRechecks(entity) && entity.getValidated()) {
			return outcome(entity, entity.getValidatedInfo() + "  NOTE:  Expected file check was skipped for this run per configuration.", true);
		}
		final String resourcePath = ResourceUtils.getResourcePath(resource);
		final List<String> resourceFiles = ResourceUtils.getRelativeFilePaths(resource);
		//log.error("resourceFiles = \n\n" + StringUtils.join(resourceFiles,"\n"));
		final List<String> expectedFiles;
		try {
			expectedFiles = getExpectedFiles(entity, session.getLabel());
			//log.error("expectedFiles = <br><br>" + StringUtils.join(expectedFiles,"\n"));
		} catch (IOException e) {
			return outcome(entity, "IOException:  Could not return expected files from EXPECTED_FILE_URL (" +
						EXPECTED_FILE_URL + ")<br><br>" + ExceptionUtils.getStackTrace(e), false);
		}
		final StringBuilder sb = new StringBuilder();
		for (final String expected : expectedFiles) {
			if (!resourceFiles.contains(expected)) {
				// The "expected" files contain directories while the resource file lists do not.  We'll check
				// if the missing file is a directory here.
				final File checkFile = new File(resourcePath + File.separator + expected);
				if (checkFile.exists()) {
					continue;
				}
				sb.append(" MissingFile:  " + expected + "<br>\n");
			}
		}
		if (sb.length()>0) {
			return outcome(entity, "Missing one or more expectedFiles (EXPECTED_FILE_URL=" + EXPECTED_FILE_URL + 
					"):  <br><br>\n\n" + sb.toString(), false);
		}
		return true;
	}

	protected boolean checkIsProcessingMarkedComplete(final PcpStatusEntity entity, XnatResourcecatalog resource, final File checkFile) {
		if (checkFile == null || !checkFile.exists()) {
			final File invalidatedFile = getInvalidatedCompletionMarkerFile(resource);
			if (invalidatedFile == null || !invalidatedFile.exists()) {
				return outcome(entity, "Completion marker file does not exist", false);
			} else {
				return outcome(entity, "Completion marker file has been invalidated " + fileInfoString(invalidatedFile), false);
			}
		}
		final File starttimeFile = ResourceUtils.getSingleFileMatchingRegex(resource, "^.*" + getPipelineName()
			+  ".starttime");
		if (starttimeFile == null || !starttimeFile.exists()) {
			return outcome(entity, "Start time marker file does not exist", false);
		}
		if (checkFile.lastModified() < starttimeFile.lastModified()) {
			return outcome(entity, "Completion marker file is older than start time marker file " 
					+ fileInfoString(checkFile), false);
		}
		try {
			if (!FileUtils.readFileToString(checkFile).startsWith("Completion Check was successful")) {
				return outcome(entity, "Completion marker contents do not indicate completion "
					+ fileInfoString(checkFile), false);
			}
		} catch (IOException e) {
			return outcome(entity, "Could not read contents of completion file", false);
		}
		return true;
	}

	private File getCompletionMarkerFile(XnatResourcecatalog resource) {
		File checkFile = ResourceUtils.getSingleFileMatchingRegex(resource, "^.*" +  ".XNAT_CHECK.success$");
		return checkFile;
	}

	private File getInvalidatedCompletionMarkerFile(XnatResourcecatalog resource) {
		File checkFile = ResourceUtils.getSingleFileMatchingRegex(resource, "^.*" +  ".XNAT_CHECK.success.invalidated$");
		return checkFile;
	}

	protected List<String> getExpectedFiles(PcpStatusEntity entity, final String expLabel) throws IOException {
		if (_expectedFileCache == null) {
			_expectedFileCache = new ArrayList<>();
			final URL expUrl = new URL(EXPECTED_FILE_URL);
			final URLConnection conn = expUrl.openConnection();
			InputStream is = conn.getInputStream();
	        InputStreamReader isr = new InputStreamReader(is);
	        BufferedReader br = new BufferedReader(isr);
	        String inputLine;
	        while ((inputLine = br.readLine()) != null) {
	        	if (inputLine.trim().startsWith("#")) {
	        		continue;
	        	}
	            _expectedFileCache.add(inputLine);
	        }
	        br.close();
		}
		final List<String> returnList = new ArrayList<>();
        for (final String inputLine : _expectedFileCache) {
        	String expectedFile = null;
        	if (entity != null) {
	            expectedFile = inputLine.replaceAll("\\{subjectid\\}", expLabel)
	            		.replaceAll("\\{scan\\}", entity.getSubGroup())
	            		.replaceAll(" ",File.separator);
	            if (!expectedFile.startsWith(expLabel + File.separator) ) {
	            	expectedFile = expLabel + File.separator + expectedFile;
	            }
        	} else {
                expectedFile = inputLine.replaceAll("\\{subjectid\\}", expLabel).replaceAll(" ",File.separator);
                if (!expectedFile.startsWith(expLabel + File.separator) ) {
                	expectedFile = expLabel + File.separator + expectedFile;
                }
        	}
        	returnList.add(expectedFile);
        }
        return returnList;
	}

	protected List<String> getExpectedFiles(final String expLabel) throws IOException {
		return getExpectedFiles(null, expLabel);
	}
	

	protected boolean outcome(PcpStatusEntity entity, String info, boolean validated) {
		entity.setValidated(false);
		entity.setValidatedTime(new Date());
		entity.setValidatedInfo(info);
		return validated;
	}
	
}
