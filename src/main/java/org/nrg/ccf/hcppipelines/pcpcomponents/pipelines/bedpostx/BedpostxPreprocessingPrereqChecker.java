package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.bedpostx;

import java.util.Arrays;
import java.util.Date;

import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class BedpostxPreprocessingPrereqChecker extends AbstractHcpPipelinesPrereqChecker {
	
	//public static final String INFOSTRING_NO_DIFFUSION_SCANS = "Session has no Diffusion scans.";
	public static final String DIFFUSION_SCAN_TYPE = "dMRI";
	
	public BedpostxPreprocessingPrereqChecker() {
		super();
		PREREQ_PIPELINE_NAME = PipelineConstants.DIFFUSION_PREPROC_PIPELINE_NAME;
		PREREQ_PIPELINE_PACKAGES = 
				Arrays.asList(new String[] { 
						"org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.diffusionpreprocessing" 
				});
	}
	
}
