package org.nrg.ccf.hcppipelines.pcpcomponents.xapi;


import org.nrg.ccf.hcppipelines.pcpcomponents.shared.HcpPipelinesExecManager;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Lazy
@XapiRestController
@Api(description = "HCP Pipelines Components API")
public class HcpPipelinesApi extends AbstractXapiRestController {
	
	@Autowired
	@Lazy
	public HcpPipelinesApi(UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
	}
	
	@ApiOperation(value = "Issues stop monitoring request to ExecManager (Any Pipeline)", 
			notes = "Issues stop monitoring request to ExecManager (Any Pipeline)",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/hcpPipelines/issueStopMonitoringRequest"}, restrictTo=AccessLevel.Member, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> issueStopMonitoringRequest() throws NrgServiceException {
		HcpPipelinesExecManager.issueStopMonitoringRequest();
		return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
	@ApiOperation(value = "Issues stop monitoring request to ExecManager (pipeline-specific)", 
			notes = "Issues stop monitoring request to ExecManager (pipeline-specific)",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/hcpPipelines/{pipeline}/issueStopMonitoringRequest"}, restrictTo=AccessLevel.Member, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> issueStopMonitoringRequest(@PathVariable("pipeline") final String pipeline) throws NrgServiceException {
		HcpPipelinesExecManager.issueStopMonitoringRequest(pipeline);
		return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
}
