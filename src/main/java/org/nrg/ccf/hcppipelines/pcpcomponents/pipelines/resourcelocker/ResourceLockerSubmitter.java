package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.resourcelocker;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.nrg.ccf.common.utilities.components.NodeUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

import com.google.common.collect.ImmutableList;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineSubmitter
public class ResourceLockerSubmitter implements PipelineSubmitterI {
	
	private final NodeUtils _nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
	final String _className = this.getClass().getSimpleName();
	
	@Override
	public List<String> getParametersYaml(final String projectId, final String pipelineId) {
		final StringBuilder sb = new StringBuilder()
				.append("operation:\n")
				.append("    id: operation\n")
				.append("    name: operation\n")
				.append("    kind: panel.select.single\n")
				.append("    value: LOCK\n")
				.append("    label: 'Operation to perform:  '\n")
				.append("    options:\n")
				.append("        'LOCK': 'Lock Resource'\n")
				.append("        'UNLOCK': 'Unlock Resource'\n");
		return ImmutableList.of(sb.toString());
	}

	@Override
	public boolean submitJob(final PcpStatusEntity entity, final PcpStatusEntityService entityService, final PipelineValidatorI validator,
			final Map<String, String> parameters, final UserI user) throws PcpSubmitException {
		
		final XnatMrsessiondata mrSession = XnatMrsessiondata.getXnatMrsessiondatasById(entity.getEntityId(), user, false);
		entity.setStatus(PcpConstants.PcpStatus.RUNNING.toString());
		final Date statusTime = new Date();
		entity.setStatusInfo("Status set to running at (TIME=" + statusTime + ", NODE=" + _nodeUtils.getXnatNode() + ")");
		entity.setStatusTime(statusTime);
		entityService.update(entity);
		if (mrSession==null) {
			final String errMsg = "ERROR:  MR Session not found for entity: " + entity.toString();
			entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(statusTime);
			entityService.update(entity);
			throw new PcpSubmitException(errMsg);
		}
		final XnatResourcecatalog targetResource = ResourceUtils.getResource(mrSession, entity.getSubGroup());
		if (targetResource == null) {
			final String errMsg = "ERROR:  Subgroup resource does not exist!";
			entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(statusTime);
			entityService.update(entity);
			throw new PcpSubmitException(errMsg);
		}
		final File resourceCatF = targetResource.getCatalogFile(CommonConstants.ROOT_PATH);
		final File resourceDirF = resourceCatF.getParentFile();
		if (!resourceDirF.getParentFile().getName().equalsIgnoreCase("RESOURCES")) {
			final String errMsg = "ERROR:  Unexpected resource file path:  (PATH=" + resourceDirF.getPath() + ")";
			entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(statusTime);
			entityService.update(entity);
			throw new PcpSubmitException(errMsg);
		}
		for (File f : FileUtils.listFilesAndDirs(resourceDirF, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE)) {
			try {
				if (lockOperationRequested(parameters)) {
					if (f.canWrite()) {
						log.trace("Locking file:  " + f.getPath());
						// NOTE: Setting writable false for all users (ugw).  They should only be user-writable,
						// however depending on how some files get into the archive, I've seen some with group
						// writability.
						f.setWritable(false, false);
					}
				} else {
					if (!f.canWrite()) {
						// NOTE: Setting writable true only for file owner
						log.trace("Unlocking file:  " + f.getPath());
						f.setWritable(true, true);
					}
				}
			} catch (PcpAutomationScriptExecutionException e) {
				final String errMsg = "ERROR:  Exception thrown.  Could not determine operation to perform: " + e.toString();
				entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
				entity.setStatusInfo(errMsg);
				entity.setStatusTime(statusTime);
				entityService.update(entity);
				throw new PcpSubmitException(errMsg);
			}
		}
		entity.setStatus(PcpConstants.PcpStatus.COMPLETE.toString());
		try {
			entity.setStatusInfo(((lockOperationRequested(parameters)) ? "Lock " : "Unlock ") + 
					"operation completed successfully.");
		} catch (PcpAutomationScriptExecutionException e) {
			entity.setStatusInfo("Unknown operation completed successfully.");
		}
		entity.setStatusTime(statusTime);
		entityService.update(entity);
		return true;
	}
	
	public boolean lockOperationRequested(Map<String, String> parameters) throws PcpAutomationScriptExecutionException {
		return (parameters.get("operation") != null && parameters.get("operation").equalsIgnoreCase("LOCK"));
	}

}
