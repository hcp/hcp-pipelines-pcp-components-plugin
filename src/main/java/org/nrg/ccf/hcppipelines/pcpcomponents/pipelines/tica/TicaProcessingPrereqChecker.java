package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.tica;

import java.util.Arrays;

import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;

@PipelinePrereqChecker
public class TicaProcessingPrereqChecker extends AbstractHcpPipelinesPrereqChecker {
	
	public TicaProcessingPrereqChecker() {
		super();
		// Switched from reapplyfix to autoreclean as prereq.  
		// LS used reapplyfix, but that was all run outside the PCP
		//PREREQ_PIPELINE_NAME = PipelineConstants.REAPPLYFIX_PIPELINE_NAME;
		PREREQ_PIPELINE_NAME = PipelineConstants.AUTORECLEAN_PIPELINE_NAME;
		PREREQ_PIPELINE_PACKAGES = 
				Arrays.asList(new String[] { 
						//"org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.reapplyfix" 
						"org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.autoreclean" 
				});
	}
	
}
