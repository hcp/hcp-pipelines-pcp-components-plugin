package org.nrg.ccf.hcppipelines.pcpcomponents.conf.newpipeline;

import java.util.Date;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xft.security.UserI;

//@PipelinePrereqChecker
public class NewPipelinePrereqChecker implements PipelinePrereqCheckerI {

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		final StringBuilder infoSB = new StringBuilder("Prerequisite checking functionality is not yet implemented.");
		if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoSB.toString())) {
			statusEntity.setPrereqs(false);
			statusEntity.setPrereqsTime(new Date());
			statusEntity.setPrereqsInfo(infoSB.toString());
		}
	}
	
}
