package org.nrg.ccf.hcppipelines.pcpcomponents.abst;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;

public abstract class AbstractHcpPipelinesSubmitter implements PipelineSubmitterI, ConfigurableComponentI {
	
	protected final String _className = this.getClass().getName().replace(".", "");

	@Override
	public List<String> getConfigurationYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"submitCommand:\n" +
				"    id: " + _className + "-submit-command\n" +
				"    name: " + _className + "-submit-command\n" +
				"    kind:  panel.input.text\n" + 
				"    label: Pipeline status check command\n" +  
				"statuscheckCommand:\n" +
				"    id: " + _className + "-statuscheck-command\n" +
				"    name: " + _className + "-statuscheck-command\n" +
				"    kind:  panel.input.text\n" + 
				"    label: Pipeline status check command\n"
				;
		returnList.add(ele);
		return returnList;
	}

}
