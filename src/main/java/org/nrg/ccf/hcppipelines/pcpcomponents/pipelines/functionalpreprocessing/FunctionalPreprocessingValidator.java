package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.functionalpreprocessing;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;

@PipelineValidator
public class FunctionalPreprocessingValidator extends AbstractHcpPipelinesValidator {
	

	public static final String PREPROC = "_preproc";
	
	public FunctionalPreprocessingValidator() {
		super();
		EXPECTED_FILE_URL = PipelineConstants.EXPECTEDFILES_FILE_URL + 
			"/FunctionalPreprocessing.txt";
	}

	@Override
	public String getPipelineName() {
		return PipelineConstants.FUNCTIONAL_PREPROC_PIPELINE_NAME;
	}

	@Override
	public String getPipelineResourceLabel(PcpStatusEntity entity) {
		return entity.getSubGroup() + PREPROC; 
	}
	
	@Override
	protected boolean isProcessingComplete(final PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource) {
		final XnatResourcecatalog strucPreprocResource = ResourceUtils.getResource(session, PipelineConstants.STRUC_PREPROC_RESOURCE_LABEL);
		if (strucPreprocResource == null) {
			return outcome(entity, "Could not open prerequisite (structural preprocessing) resource", false);
		}
		checkCompletionFileModTime(entity, resource, strucPreprocResource);
		return checkExpectedFiles(entity, session, resource);
	}
	
}
