package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.multirunicafix;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.dto.ExecutionGroup;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelinePrereqChecker
public class MultiRunIcaFixPrereqChecker extends AbstractHcpPipelinesPrereqChecker {
	
	final PcpUtils _pcpUtils = XDAT.getContextService().getBean(PcpUtils.class);

	public MultiRunIcaFixPrereqChecker() {
		super();
		PREREQ_PIPELINE_NAME = PipelineConstants.FUNCTIONAL_PREPROC_PIPELINE_NAME;
		PREREQ_PIPELINE_PACKAGES = Arrays.asList(new String[] { "org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.functionalpreprocessing" });
	}

	@Override
	public void checkPrereqs(PcpStatusEntity entity, UserI user) {
		
		final String resourceLockerPipeline = getResourceLockerPipeline(entity.getProject());
		List<PcpStatusEntity> resourceLockerEntities = _statusEntityService.getStatusEntities(entity.getProject(), 
				resourceLockerPipeline, entity.getEntityId());
		boolean setImpeded = false;
		if (resourceLockerEntities.size()>0) {
			for (final PcpStatusEntity resourceLockerEntity : resourceLockerEntities) {
				try {
					final PipelineValidatorI validator = _pcpUtils.getComponentSet(entity).getValidator();
					if (validator instanceof AbstractHcpPipelinesValidator) {
						final AbstractHcpPipelinesValidator pipelinesValidator = (AbstractHcpPipelinesValidator)validator;
						if (resourceLockerEntity.getSubGroup().equals(pipelinesValidator.getPipelineResourceLabel(entity))) {
							if (resourceLockerEntity.getValidated()) {
								setImpeded = true;
								if (!entity.getImpeded() || (entity.getImpeded() && entity.getImpededInfo().equals(PREREQ_IMPEDED_INFO))) {
									entity.setImpeded(true);
									entity.setImpededInfo(VALIDATION_IMPEDED_INFO);
									entity.setImpededTime(new Date());
								}
							} else if (resourceLockerEntity.getPrereqs()) {
								setImpeded = true;
								if (!entity.getImpeded() || (entity.getImpeded() && entity.getImpededInfo().equals(VALIDATION_IMPEDED_INFO))) {
									entity.setImpeded(true);
									entity.setImpededInfo(PREREQ_IMPEDED_INFO);
									entity.setImpededTime(new Date());
								}
							}
						}
					}
				} catch (PcpComponentSetException e) {
					log.error("Could not get component set for entity " + entity);
				}
			}
		}
		// Let's work with a clone here in case the prereq checker sets any values.
		final List<String> exclusions = getExcludedEntities(entity, user);
		if (exclusions.contains(entity.getEntityLabel())) {
			final String infoStr = "Entity has been excluded by configuration"; 
			if (entity.getPrereqs() || !entity.getPrereqsInfo().equals(infoStr.toString())) {
				entity.setPrereqs(false);
				entity.setPrereqsTime(new Date());
				entity.setPrereqsInfo(infoStr.toString());
			} 
			return;
		}
		if (!setImpeded && entity.getImpeded() && (entity.getImpededInfo().equals(VALIDATION_IMPEDED_INFO) || entity.getImpededInfo().equals(PREREQ_IMPEDED_INFO))) {
			entity.setImpeded(false);
			entity.setImpededInfo("");
			entity.setImpededTime(new Date());
		}
		// NOTE:  We can't afford to recalculate FUNCTIONALPREPROC validation, so we'll just use the validation from the FUNCTIONAL pipeline
		final String functionalPreprocPipeline = getPrereqPipeline(entity.getProject());
		if (functionalPreprocPipeline == null || functionalPreprocPipeline.isEmpty()) {
			if (entity.getPrereqs() || entity.getPrereqsInfo().isEmpty()) {
				entity.setPrereqs(false);
				entity.setPrereqsInfo("Functional Preprocessing pipeline is not configured for this project.  " +
						"Status is determined from Functional Preprocessing pipeline validation.");
				entity.setPrereqsTime(new Date());
			}
			return;
		}
		XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(entity.getEntityId(), user, false);
		List<XnatResourcecatalog> unprocResources = ResourceUtils.getResourcesMatchingRegex(session, PipelineConstants.FUNCTIONAL_UNPROC_REGEX);
		boolean allValidationOk = true;
		final StringBuilder failedSB = new StringBuilder();
		for ( XnatAbstractresourceI resource : unprocResources) {
			final String resourceLabel = resource.getLabel();
			final String GROUP = resourceLabel.replace("_unproc", "");
			
			final PcpStatusEntity functionalPreprocEntity = _statusEntityService.getStatusEntity(entity.getProject(), functionalPreprocPipeline, new ExecutionGroup(entity.getEntityId(), GROUP)); 
			if (functionalPreprocEntity == null || functionalPreprocEntity.getValidated() == null) {
				entity.setPrereqs(false);
				final String funcInfo = (functionalPreprocEntity != null && functionalPreprocEntity.getValidatedInfo() != null) ? 
						functionalPreprocEntity.getValidatedInfo() : "";
				entity.setPrereqsInfo("Could not get validation status from Functional Preprocessing pipeline entity (GROUP=" + GROUP +
						")\n\n" + funcInfo);
				entity.setPrereqsTime(new Date());
				allValidationOk = false;
				return;
			}
			if ((entity.getPrereqs() == null || !entity.getPrereqs()) && functionalPreprocEntity.getValidated()) {
				// Do nothing for now
			} else if ((entity.getPrereqs() || entity.getPrereqsInfo().isEmpty()) && !functionalPreprocEntity.getValidated() || (entity.getPrereqsInfo() == null || 
					!entity.getPrereqsInfo().contains(functionalPreprocEntity.getValidatedInfo()))) {
				entity.setPrereqs(functionalPreprocEntity.getValidated());
				if (!functionalPreprocEntity.getValidated()) {
					allValidationOk = false;
					failedSB.append((failedSB.length()>0) ? "," + GROUP : GROUP);
				} else {
					// Do nothing for now
				}
			}
		}
		if (allValidationOk) {
			entity.setPrereqs(true);
			entity.setPrereqsInfo("Functional Preprocessing pipeline reports successful validation for all processing groups");
			entity.setPrereqsTime(new Date());
		} else  {
			entity.setPrereqs(false);
			entity.setPrereqsInfo("Functional Preprocessing pipeline reports that validation failed for one or more processing groups:\n\n" +
					failedSB.toString());
			entity.setPrereqsTime(new Date());
		}
		return;
	}
	
}
