package org.nrg.ccf.hcppipelines.pcpcomponents.shared;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xft.security.UserI;

@PipelineSubmitter
public class HcpPipelinesSubmitter implements PipelineSubmitterI {

	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"queuedLimit:\n" +
				"    id: queuedLimit\n" +
				"    kind: panel.input.text\n" +
				"    label: Queued limit\n" +
				"    description: Maximum number of queued jobs for this pipeline\n" +
				"    value: 20\n" +
				"pipelineArgs:\n" +
				"    id: pipelineArgs\n" +
				"    kind: panel.input.text\n" +
				"    size: 50\n" +
				"    label: Pipeline arguments\n" +
				"    placeholder: '-bigventricles|-autorecon2-wm|-autorecon3' \n" +
				"    description: NOTE:  These must be written into the pipeline runner.  Arguments are currently " +
				     "only defined to pass additional freesurfer parameters to structural preprocessing\n" +
				"overrideQueuedLimit:\n" +
				"    id: overrideQueuedLimit\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Override queued limit?\n" +
				"    description: Normally, jobs will not be queued if the current number of queued jobs exceeds the " +
					"queued limit (currently 20).  Occasionally one may desire to queue a small number of runs " +
					"while another larger batch job is running.  This option allows for that.\n" +
				"    value: false\n" +
				"    onText: Yes\n" +
				"    offText: No\n" +
				"generateCinabStructure:\n" +
				"    id: generateCinabStructure\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Generate CinaB Structure?\n" +
				"    description: (Requires CinaB pipeline and package configuration) " +
					"Automatically (re)generate CinaB structure upon pipeline completion. \n" +
				"    value: false\n" +
				"    onText: Yes\n" +
				"    offText: No\n" +
				"resetCurrentActive:\n" +
				"    id: resetCurrentActive\n" +
				"    kind: panel.input.switchbox\n" +
				"    label: Reset current active jobs?\n" +
				"    description: Sometimes a network/machine blip can cause sessions to move into an error state because " +
					"the running/queued lists come back as empty when, in fact, jobs are still active. " +
					"Other times a Tomcat restart may require resubmission.  This option can be used to link back up to jobs " +
					"currently queued or running on the server.  Then their status is monitored and reported.  " +
					"<em>NOTE: Queued or running sessions will not actually be submitted when this option is used.  " +
					"Rather, they will be indicated as submitted and their status will be monitored as if they were submitted in " +
					"the current session.</em>\n" +
				"    value: false\n" +
				"    onText: Yes\n" +
				"    offText: No\n";
		returnList.add(ele);
		return returnList;
	}

	@Override
	public boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator,
			Map<String, String> parameters, UserI user) throws PcpSubmitException {
		// The exec manager will do the actual work of submission
		return true;
	}

}
