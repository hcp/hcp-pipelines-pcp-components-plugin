package org.nrg.ccf.hcppipelines.pcpcomponents.abst;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AbstractHcpPipelinesPrereqChecker implements PipelinePrereqCheckerI, ConfigurableComponentI {
	
	protected final ConfigService _configService = XDAT.getConfigService();
	protected final PcpStatusEntityService _statusEntityService = XDAT.getContextService().getBean(PcpStatusEntityService.class);
	protected final Gson _gson = new Gson();
	private final Map<String,List<String>> _configCache = new HashMap<>();
	protected final Map<String,String> PREREQ_PIPELINE_MAP = new HashMap<>();
	protected final Map<String,String> RESOURCELOCKER_PIPELINE_MAP = new HashMap<>();
	protected final Map<String,Configuration> CURRENT_PIPELINE_MAP = new HashMap<>();
	protected List<String> PREREQ_PIPELINE_PACKAGES;
	protected String PREREQ_PIPELINE_NAME;
	private List<String> _configYaml = null;
	protected final String _className = this.getClass().getName().replace(".", "");
	final PcpUtils _pcpUtils = XDAT.getContextService().getBean(PcpUtils.class);
	protected final static String VALIDATION_IMPEDED_INFO = "ResourceLocker validator indicates that resource is locked for this pipeline";
	protected final static String PREREQ_IMPEDED_INFO = "ResourceLocker prereqs checker indicates that resource SHOULD BE LOCKED for this pipeline " 
			+ "but the validator indicates it IS NOT LOCKED!!!";

	@Override
	public List<String> getConfigurationYaml() {
		if (_configYaml != null) {
			return _configYaml;
		}
		_configYaml = new ArrayList<>();
		final StringBuilder sb = new StringBuilder();
		sb.append(_className)
		.append("ExcludedEntities:\n")
		.append("    id: " + _className + "-excluded-entities\n")
		.append("    name: " + _className + "-excluded-entities\n")
		.append("    kind: panel.textarea\n")
		.append("    label: Excluded entities:\n")
		.append("    description: List of entities to be exluded by configuration (PrereqsMet=No).  These can be listed as [subgroup,subgroup2]:[entity] " + 
				"if entities should only be excluded for certain subgroups.\n");
		_configYaml.add(sb.toString());
		return _configYaml;
	}
	
	
	protected List<String> getConfiguredExclusions(String project, String pipeline) throws PcpAutomationScriptExecutionException {
		final String configValue = PcpComponentUtils.getConfigValue(project, pipeline, this.getClass().getName(),
				"-excluded-entities", false).toString();
		if (!_configCache.containsKey(project)) {
			final List<String> returnLst = new ArrayList<>();
			final String configArr[] = configValue.split("\\r?\\n");
			for (String configV : configArr) {
				if (configV == null) {
					continue;
				}
				configV = configV.trim();
				if (configV.length()>0 && !returnLst.contains(configV)) {
					returnLst.add(configV);
				}
			}
			_configCache.put(project, returnLst);
		}
		return _configCache.get(project);
	}

	@Override
	public void checkPrereqs(PcpStatusEntity entity, UserI user) {
		final String resourceLockerPipeline = getResourceLockerPipeline(entity.getProject());
		List<PcpStatusEntity> resourceLockerEntities = _statusEntityService.getStatusEntities(entity.getProject(), 
				resourceLockerPipeline, entity.getEntityId());
		boolean setImpeded = false;
		if (resourceLockerEntities.size()>0) {
			for (final PcpStatusEntity resourceLockerEntity : resourceLockerEntities) {
				try {
					final PipelineValidatorI validator = _pcpUtils.getComponentSet(entity).getValidator();
					if (validator instanceof AbstractHcpPipelinesValidator) {
						final AbstractHcpPipelinesValidator pipelinesValidator = (AbstractHcpPipelinesValidator)validator;
						if (resourceLockerEntity.getSubGroup().equals(pipelinesValidator.getPipelineResourceLabel(entity))) {
							if (resourceLockerEntity.getValidated()) {
								setImpeded = true;
								if (!entity.getImpeded() || (entity.getImpeded() && entity.getImpededInfo().equals(PREREQ_IMPEDED_INFO))) {
									entity.setImpeded(true);
									entity.setImpededInfo(VALIDATION_IMPEDED_INFO);
									entity.setImpededTime(new Date());
								}
							} else if (resourceLockerEntity.getPrereqs()) {
								setImpeded = true;
								if (!entity.getImpeded() || (entity.getImpeded() && entity.getImpededInfo().equals(VALIDATION_IMPEDED_INFO))) {
									entity.setImpeded(true);
									entity.setImpededInfo(PREREQ_IMPEDED_INFO);
									entity.setImpededTime(new Date());
								}
							}
						}
					}
				} catch (PcpComponentSetException e) {
					log.error("Could not get component set for entity " + entity);
				}
			}
		}
		// Let's work with a clone here in case the prereq checker sets any values.
		final List<String> exclusions = getExcludedEntities(entity, user);
		if (exclusions.contains(entity.getEntityLabel())) {
			final String infoStr = "Entity has been excluded by configuration"; 
			if (entity.getPrereqs() || !entity.getPrereqsInfo().equals(infoStr.toString())) {
				entity.setPrereqs(false);
				entity.setPrereqsTime(new Date());
				entity.setPrereqsInfo(infoStr.toString());
			} 
			return;
		}
		if (!setImpeded && entity.getImpeded() && (entity.getImpededInfo().equals(VALIDATION_IMPEDED_INFO) || entity.getImpededInfo().equals(PREREQ_IMPEDED_INFO))) {
			entity.setImpeded(false);
			entity.setImpededInfo("");
			entity.setImpededTime(new Date());
		}
		final String prereqPipeline = getPrereqPipeline(entity.getProject());
		if (prereqPipeline == null || prereqPipeline.isEmpty()) {
			if (entity.getPrereqs() || entity.getPrereqsInfo().isEmpty()) {
				entity.setPrereqs(false);
				entity.setPrereqsInfo(PREREQ_PIPELINE_NAME + " is not configured for this project.  " +
						"Status is determined from " + PREREQ_PIPELINE_NAME + " validation.");
				entity.setPrereqsTime(new Date());
			}
			return;
		}
		List<PcpStatusEntity> prereqEntities = _statusEntityService.getStatusEntities(entity.getProject(), 
				prereqPipeline, entity.getEntityId());
		final PcpStatusEntity prereqEntity;
		if (prereqEntities.size() == 1) {
			prereqEntity = prereqEntities.get(0);
		} else if (prereqEntities.size()>1) {
			entity.setPrereqs(false);
			entity.setPrereqsInfo(PREREQ_PIPELINE_NAME + " returned multiple status entity records. " +
					"A subgroup must be specified, which may require a custom prereq checker.");
			entity.setPrereqsTime(new Date());
			return;
		} else {
			prereqEntity = null;
		}
		if (prereqEntity == null || prereqEntity.getValidated() == null) {
			entity.setPrereqs(false);
			entity.setPrereqsInfo("Could not get validation status from " + PREREQ_PIPELINE_NAME + " entity\n\n" + 
					((prereqEntity != null && prereqEntity.getValidatedInfo()!=null) ? 
							prereqEntity.getValidatedInfo() : ""));
			entity.setPrereqsTime(new Date());
			return;
		}
		if ((entity.getPrereqs() == null || !entity.getPrereqs()) && prereqEntity.getValidated()) {
			entity.setPrereqs(true);
			entity.setPrereqsInfo(PREREQ_PIPELINE_NAME + " reports successful validation");
			entity.setPrereqsTime(new Date());
			
		} else if ((entity.getPrereqs() || entity.getPrereqsInfo().isEmpty()) && !prereqEntity.getValidated() || (entity.getPrereqsInfo() == null || 
				!entity.getPrereqsInfo().contains(prereqEntity.getValidatedInfo()))) {
			entity.setPrereqs(prereqEntity.getValidated());
			if (!prereqEntity.getValidated()) {
				entity.setPrereqsInfo(PREREQ_PIPELINE_NAME + " reports validation failed:\n\n" + prereqEntity.getValidatedInfo());
			} else {
				entity.setPrereqsInfo(PREREQ_PIPELINE_NAME + " reports successful validation");
			}
			entity.setPrereqsTime(new Date());
		}
		return;
	}
	
	protected List<String> getExcludedEntities(PcpStatusEntity statusEntity, UserI user) {
		try {
			return populateReturnList(statusEntity, getConfiguredExclusions(statusEntity.getProject(), statusEntity.getPipeline()));
		} catch (PcpAutomationScriptExecutionException e) {
			log.error("Exception thrown retrieving configured exclusions:", e);
			return new ArrayList<>();
		}
	}
	

	private List<String> populateReturnList(PcpStatusEntity statusEntity, List<String> exclusions) {
		final List<String> returnLst = new ArrayList<>();
		for (final String configV : exclusions) {
			if (configV == null) {
				continue;
			}
			if (configV.contains(":")) {
				final String valA[] = configV.split(":");
				String subG = valA[0];
				String subV = valA[1];
				if (subG == null || subV == null) {
					continue;
				}
				subG = subG.trim();
				subV = subV.trim();
				for (final String subGG : subG.split(",")) {
					if ((subGG).equals(statusEntity.getSubGroup())) {
						if (!returnLst.contains(subV)) {
							returnLst.add(subV);
						}
					}
				}
			} else {
				if (configV.trim().length()>0 && !returnLst.contains(configV)) {
					returnLst.add(configV);
				}
			}
		}
		return returnLst;
	}

	
	protected String getPrereqPipeline(final String project) {
		if (!PREREQ_PIPELINE_MAP.containsKey(project)) {
			final Configuration conf = _configService.getConfig(PcpConfigConstants.CONFIG_TOOL, PcpConfigConstants.CONFIG_PATH, Scope.Project, project);
			final List<PcpConfigInfo> projectConfigs;
			if (conf != null) {
				projectConfigs = _gson.fromJson(conf.getContents(),new TypeToken<List<PcpConfigInfo>>(){}.getType());
			} else {
				projectConfigs = new ArrayList<>();
			}
			outerLoop:
			for (final PcpConfigInfo projectConfig : projectConfigs) {
				for (final String prereqPackage : PREREQ_PIPELINE_PACKAGES) {
					if (projectConfig.getValidator().startsWith(prereqPackage + ".")) {
						PREREQ_PIPELINE_MAP.put(project, projectConfig.getPipeline());
						break outerLoop;
					}
				}
			}
		}
		return PREREQ_PIPELINE_MAP.get(project);
	}
	
	protected String getResourceLockerPipeline(final String project) {
		if (!RESOURCELOCKER_PIPELINE_MAP.containsKey(project)) {
			final Configuration conf = _configService.getConfig(PcpConfigConstants.CONFIG_TOOL, PcpConfigConstants.CONFIG_PATH, Scope.Project, project);
			final List<PcpConfigInfo> projectConfigs;
			if (conf != null) {
				projectConfigs = _gson.fromJson(conf.getContents(),new TypeToken<List<PcpConfigInfo>>(){}.getType());
			} else {
				projectConfigs = new ArrayList<>();
			}
			for (final PcpConfigInfo projectConfig : projectConfigs) {
				if (projectConfig.getValidator().contains("ResourceLocker")) {
					RESOURCELOCKER_PIPELINE_MAP.put(project, projectConfig.getPipeline());
					break;
				}
			}
		}
		return RESOURCELOCKER_PIPELINE_MAP.get(project);
	}

}
