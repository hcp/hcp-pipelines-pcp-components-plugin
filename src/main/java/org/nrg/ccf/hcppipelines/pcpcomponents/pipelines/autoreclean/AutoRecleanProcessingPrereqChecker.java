package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.autoreclean;

import java.util.Arrays;

import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;

@PipelinePrereqChecker
public class AutoRecleanProcessingPrereqChecker extends AbstractHcpPipelinesPrereqChecker {
	
	public AutoRecleanProcessingPrereqChecker() {
		super();
		PREREQ_PIPELINE_NAME = PipelineConstants.MSMALL_PIPELINE_NAME;
		PREREQ_PIPELINE_PACKAGES = 
				Arrays.asList(new String[] { 
						"org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.msmall" 
				});
	}
	
}
