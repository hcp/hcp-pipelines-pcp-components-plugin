package org.nrg.ccf.hcppipelines.pcpcomponents.pojo;

public enum SubmissionStatus {
	SUBMITTED, QUEUED, RUNNING, ERROR, VALIDATED, NOT_ACTIVE, UNKNOWN
}
