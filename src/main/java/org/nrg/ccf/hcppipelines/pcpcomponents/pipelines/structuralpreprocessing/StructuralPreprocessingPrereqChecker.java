package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.structuralpreprocessing;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.config.services.ConfigService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelinePrereqChecker
public class StructuralPreprocessingPrereqChecker extends AbstractHcpPipelinesPrereqChecker implements PipelinePrereqCheckerI, ConfigurableComponentI {
		
	protected String T1_REGEX;
	protected String T2_REGEX;
	protected final String _className = this.getClass().getName().replace(".", "");
	
	protected final Gson _gson = new Gson();
	protected final PcpStatusEntityService _statusEntityService = XDAT.getContextService().getBean(PcpStatusEntityService.class);
	protected final ConfigService _configService = XDAT.getConfigService();
	protected final Map<String,String> RESOURCELOCKER_PIPELINE_MAP = new HashMap<>();
	final PcpUtils _pcpUtils = XDAT.getContextService().getBean(PcpUtils.class);
	protected final static String VALIDATION_IMPEDED_INFO = "ResourceLocker validator indicates that resource is locked for this pipeline";
	protected final static String PREREQ_IMPEDED_INFO = "ResourceLocker prereqs checker indicates that resource SHOULD BE LOCKED for this pipeline " 
			+ "but the validator indicates it IS NOT LOCKED!!!";
	
	@Override
	public List<String> getConfigurationYaml() {
		List<String> configYaml = new ArrayList<>();
		final StringBuilder sb = new StringBuilder()
				.append("t1Regex:\n")
				.append("    id: " + _className + "-t1-regex\n")
				.append("    name: " + _className + "-t1-regex\n")
				.append("    kind: panel.input.text\n")
				.append("    placeholder: " + PipelineConstants.T1_REGEX + "\n")
				.append("    label: Regular expression for matching T1 unproc resources\n")
				.append("t2Regex:\n")
				.append("    id: " + _className + "-t2-regex\n")
				.append("    name: " + _className + "-t2-regex\n")
				.append("    kind: panel.input.text\n")
				.append("    placeholder: " + PipelineConstants.T2_REGEX + "\n")
				.append("    label: Regular expression for matching T2 unproc resources\n")
				;
		configYaml.add(sb.toString());
		configYaml.addAll(super.getConfigurationYaml());
		return configYaml;
	}

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		
		final String resourceLockerPipeline = getResourceLockerPipeline(statusEntity.getProject());
		List<PcpStatusEntity> resourceLockerEntities = _statusEntityService.getStatusEntities(statusEntity.getProject(), 
				resourceLockerPipeline, statusEntity.getEntityId());
		boolean setImpeded = false;
		if (resourceLockerEntities.size()>0) {
			for (final PcpStatusEntity resourceLockerEntity : resourceLockerEntities) {
				try {
					final PipelineValidatorI validator = _pcpUtils.getComponentSet(statusEntity).getValidator();
					if (validator instanceof AbstractHcpPipelinesValidator) {
						final AbstractHcpPipelinesValidator pipelinesValidator = (AbstractHcpPipelinesValidator)validator;
						if (resourceLockerEntity.getSubGroup().equals(pipelinesValidator.getPipelineResourceLabel(statusEntity))) {
							if (resourceLockerEntity.getValidated()) {
								setImpeded = true;
								if (!statusEntity.getImpeded() || (statusEntity.getImpeded() && statusEntity.getImpededInfo().equals(PREREQ_IMPEDED_INFO))) {
									statusEntity.setImpeded(true);
									statusEntity.setImpededInfo(VALIDATION_IMPEDED_INFO);
									statusEntity.setImpededTime(new Date());
								}
							} else if (resourceLockerEntity.getPrereqs()) {
								setImpeded = true;
								if (!statusEntity.getImpeded() || (statusEntity.getImpeded() && statusEntity.getImpededInfo().equals(VALIDATION_IMPEDED_INFO))) {
									statusEntity.setImpeded(true);
									statusEntity.setImpededInfo(PREREQ_IMPEDED_INFO);
									statusEntity.setImpededTime(new Date());
								}
							}
						}
					}
				} catch (PcpComponentSetException e) {
					log.error("Could not get component set for entity " + statusEntity);
				}
			}
		}
		// Let's work with a clone here in case the prereq checker sets any values.
		final List<String> exclusions = getExcludedEntities(statusEntity, user);
		if (exclusions.contains(statusEntity.getEntityLabel())) {
			final String infoStr = "Entity has been excluded by configuration"; 
			if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoStr.toString())) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo(infoStr.toString());
			} 
			return;
		}
		if (!setImpeded && statusEntity.getImpeded() && (statusEntity.getImpededInfo().equals(VALIDATION_IMPEDED_INFO) || statusEntity.getImpededInfo().equals(PREREQ_IMPEDED_INFO))) {
			statusEntity.setImpeded(false);
			statusEntity.setImpededInfo("");
			statusEntity.setImpededTime(new Date());
		}
		
		try {
			T1_REGEX = PcpComponentUtils.getConfigValue(statusEntity.getProject(), statusEntity.getPipeline(), 
							this.getClass().getName(), "-t1-regex", false).toString();
			T2_REGEX = PcpComponentUtils.getConfigValue(statusEntity.getProject(), statusEntity.getPipeline(), 
						this.getClass().getName(), "-t2-regex", false).toString();
		} catch (PcpAutomationScriptExecutionException e) {
			log.warn("Exception thrown obtaining config values (T1_REGEX, T2_REGEX");
		}
		if (T1_REGEX == null || T1_REGEX.isEmpty()) {
			T1_REGEX = PipelineConstants.T1_REGEX;
		}
		if (T2_REGEX == null || T2_REGEX.isEmpty()) {
			T2_REGEX = PipelineConstants.T2_REGEX;
		}
		
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		boolean t1match = false;
		boolean t2match = false;
		for (final XnatAbstractresourceI resource : session.getResources_resource()) {
			if (resource.getLabel().matches(T1_REGEX) && resource.getFileCount()>1) {
				t1match = true;
			} else if (resource.getLabel().matches(T2_REGEX) && resource.getFileCount()>1) {
				t2match = true;
			}
			if (t1match && t2match) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo("Session contains structural unproc resources");
				return;
			}
		}
		final StringBuilder infoSB = new StringBuilder();
		if (!t1match) {
			infoSB.append("Structural T1 resource is missing or empty.   ");
		}
		if (!t2match) {
			infoSB.append("Structural T2 resource is missing or empty.   ");
		}
		statusEntity.setPrereqs(false);
		statusEntity.setPrereqsTime(new Date());
		statusEntity.setPrereqsInfo(infoSB.toString());
	}
	
}
