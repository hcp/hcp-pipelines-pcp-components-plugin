package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.autoreclean;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@PipelineValidator
public class AutoRecleanProcessingValidator extends AbstractHcpPipelinesValidator {
	
	public AutoRecleanProcessingValidator() {
		super();
		EXPECTED_FILE_URL = PipelineConstants.EXPECTEDFILES_FILE_URL + 
			"/AutoRecleanProcessing.txt";
	}

	@Override
	public String getPipelineName() {
		return PipelineConstants.AUTORECLEAN_PIPELINE_NAME;
	}

	@Override
	public String getPipelineResourceLabel(PcpStatusEntity entity) {
		return PipelineConstants.AUTORECLEAN_RESOURCE_LABEL;
	}
	
	@Override
	protected boolean isProcessingComplete(final PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource) {
		final XnatResourcecatalog procResource = ResourceUtils.getResource(session, PipelineConstants.MSMALL_RESOURCE_LABEL);
		if (procResource == null) {
			return outcome(entity, "Could not open prerequisite (MSMAll) resource", false);
		}
		checkCompletionFileModTime(entity, resource, procResource);
		return checkExpectedFiles(entity, session, resource);
	}

}
