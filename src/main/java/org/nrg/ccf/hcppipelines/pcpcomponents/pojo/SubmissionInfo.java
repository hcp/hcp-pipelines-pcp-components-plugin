package org.nrg.ccf.hcppipelines.pcpcomponents.pojo;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubmissionInfo {
	
	private Date submissionTime;
	private Date lastUpdateTime;
	private SubmissionStatus submissionStatus;

	public static SubmissionInfo newSubmission() {
		Date currentTime = new Date();
		final SubmissionInfo submission = new SubmissionInfo();
		submission.setSubmissionTime(currentTime);
		submission.setLastUpdateTime(currentTime);
		submission.setSubmissionStatus(SubmissionStatus.SUBMITTED);
		return submission;
	}

	public static SubmissionInfo newFailedSubmission() {
		final SubmissionInfo submission = newSubmission();
		submission.setSubmissionStatus(SubmissionStatus.ERROR);
		return submission;
	}

}
