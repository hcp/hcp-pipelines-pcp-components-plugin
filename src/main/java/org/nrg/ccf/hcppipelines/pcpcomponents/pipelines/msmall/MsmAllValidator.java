package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.msmall;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesValidator;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@PipelineValidator
public class MsmAllValidator extends AbstractHcpPipelinesValidator {
	
	public MsmAllValidator() {
		super();
		EXPECTED_FILE_URL = PipelineConstants.EXPECTEDFILES_FILE_URL + 
			"/MsmAllProcessing.txt";
	}

	@Override
	public String getPipelineName() {
		return PipelineConstants.MSMALL_PIPELINE_NAME;
	}

	@Override
	public String getPipelineResourceLabel(PcpStatusEntity entity) {
		return PipelineConstants.MSMALL_RESOURCE_LABEL;
	}
	
	@Override
	protected boolean isProcessingComplete(final PcpStatusEntity entity, XnatMrsessiondata session, XnatResourcecatalog resource) {
		final XnatResourcecatalog icaFixResource = ResourceUtils.getResource(session, PipelineConstants.MULTIRUNICAFIX_RESOURCE_LABEL);
		if (icaFixResource == null) {
			return outcome(entity, "Could not open prerequisite (MultiRunIcaFix) resource", false);
		}
		checkCompletionFileModTime(entity, resource, icaFixResource);
		return checkExpectedFiles(entity, session, resource);
	}

}
