package org.nrg.ccf.hcppipelines.pcpcomponents.projectspecific;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.utils.LifespanUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.diffusionpreprocessing.DiffusionPreprocessingPrereqChecker;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class DiffusionPreprocessingPrereqChecker_LS extends DiffusionPreprocessingPrereqChecker {
	
	public static final List<String> _excludedSites = Arrays.asList(new String[] { "Harvard", "UMN" });

	
	public DiffusionPreprocessingPrereqChecker_LS() {
		super();
		PREREQ_PIPELINE_NAME = PipelineConstants.STRUC_PREPROC_PIPELINE_NAME;
		PREREQ_PIPELINE_PACKAGES = 
				Arrays.asList(new String[] { 
						"org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.structuralpreprocessing" 
				});
	}
	
	@Override
	public void checkPrereqs(PcpStatusEntity entity, UserI user) {
		super.checkPrereqs(entity, user);
		// Currently not processing Harvard, UMinn scans.
		if (entity.getPrereqs() == null || entity.getPrereqs()) {
			XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(entity.getEntityId(), user, false);
			final String scanner = session.getScanner();
			if (scanner == null) {
				entity.setPrereqs(false);
				entity.setPrereqsInfo("Scanner field is null:  Cannot determine site");
				entity.setPrereqsTime(new Date());
				return;
			}
			final String site = LifespanUtils.getLifespanSiteFromScannerInfo(scanner);
			if (_excludedSites.contains(site)) {
				entity.setPrereqs(false);
				entity.setPrereqsInfo( site + " Session:  Not yet running.");
				entity.setPrereqsTime(new Date());
			}
		}
	}

}
