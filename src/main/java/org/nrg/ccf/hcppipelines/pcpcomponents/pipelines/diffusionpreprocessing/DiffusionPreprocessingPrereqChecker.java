package org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.diffusionpreprocessing;

import java.util.Arrays;
import java.util.Date;

import org.nrg.ccf.hcppipelines.pcpcomponents.abst.AbstractHcpPipelinesPrereqChecker;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

@PipelinePrereqChecker
public class DiffusionPreprocessingPrereqChecker extends AbstractHcpPipelinesPrereqChecker {
	
	public static final String INFOSTRING_NO_DIFFUSION_SCANS = "Session has no Diffusion scans.";
	public static final String DIFFUSION_SCAN_TYPE = "dMRI";
	
	public DiffusionPreprocessingPrereqChecker() {
		super();
		PREREQ_PIPELINE_NAME = PipelineConstants.STRUC_PREPROC_PIPELINE_NAME;
		PREREQ_PIPELINE_PACKAGES = 
				Arrays.asList(new String[] { 
						"org.nrg.ccf.hcppipelines.pcpcomponents.pipelines.structuralpreprocessing" 
				});
	}
	
	@Override
	public void checkPrereqs(PcpStatusEntity entity, UserI user) {
		super.checkPrereqs(entity, user);
		if (entity.getPrereqs() == null || entity.getPrereqs()) {
			XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(entity.getEntityId(), user, false);
			boolean hasDmri = false;
			for (final XnatImagescandataI scan : session.getScans_scan()) {
				if (scan.getType().equalsIgnoreCase(DIFFUSION_SCAN_TYPE)) {
					hasDmri = true;
					break;
				}
			}
			if (!hasDmri) {
				entity.setPrereqs(false);
				entity.setPrereqsInfo(INFOSTRING_NO_DIFFUSION_SCANS);
				entity.setPrereqsTime(new Date());
				if (entity.getImpeded() == null || !entity.getImpeded()) {
					entity.setImpeded(true);
					entity.setImpededInfo(INFOSTRING_NO_DIFFUSION_SCANS);
					entity.setImpededTime(new Date());
				}
				return;
			}
		}
	}

}
